#pragma once
#include <SimpleMath.h>
#include <vector>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace DirectX::SimpleMath;
class Blob
{
public:
	
	int id;
	const float mass = 0.01f;
	const float radius = 0.03f;
	float volume;
	Vector3 velocity = Vector3(0, 0, 0);
	Vector3 position;
	float pressure;
	Vector3 force = Vector3(0, 0, 0);
	float density;
	float restDensity = 200;
	std::vector<Blob*> neighbours;

	Blob(Vector3 pos);
	Blob();
	~Blob();

	float calculateDensity(std::vector<Blob*> blobsToInterpolate, float kernelSize);
	float calculatePressure(float stiffness);
	Vector3 calculatePressureForce(std::vector<Blob*> blobsToInterpolate, float kernelSize, int thisBlobIndex);
};

