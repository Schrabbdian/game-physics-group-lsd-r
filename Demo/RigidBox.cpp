#include "RigidBox.h"

void RigidBox::configureDraw(DirectX::BasicEffect* effect)//actual draw call has to happen right after this method is called
{
	// Set attributes in shader
	effect->SetDiffuseColor(XMVectorSet(color.x, color.y, color.z, 1));//color

	effect->SetWorld(this->getWorldMatrix());
}

void RigidBox::update(float timeStep)
{
	rigidBody.update(timeStep);

	position = rigidBody.getPosition();
}

RigidBox::RigidBox()
{
}

RigidBox::RigidBox(XMFLOAT3 position, XMFLOAT3 scale, float mass, XMFLOAT3 rotation, XMFLOAT3 linearVelocity, XMFLOAT3 angularMomentum)
{

	this->position = position;
	this->scale = scale;
	this->mass = mass;

	std::vector<Point> points;
	points.clear();

	points.push_back(Point(position + XMFLOAT3( scale.x / 2,  scale.y / 2,  scale.z / 2), mass / 8));
	points.push_back(Point(position + XMFLOAT3( scale.x / 2,  scale.y / 2, -scale.z / 2), mass / 8));
	points.push_back(Point(position + XMFLOAT3( scale.x / 2, -scale.y / 2,  scale.z / 2), mass / 8));
	points.push_back(Point(position + XMFLOAT3( scale.x / 2, -scale.y / 2, -scale.z / 2), mass / 8));

	points.push_back(Point(position + XMFLOAT3(-scale.x / 2,  scale.y / 2,  scale.z / 2), mass / 8));
	points.push_back(Point(position + XMFLOAT3(-scale.x / 2,  scale.y / 2, -scale.z / 2), mass / 8));
	points.push_back(Point(position + XMFLOAT3(-scale.x / 2, -scale.y / 2,  scale.z / 2), mass / 8));
	points.push_back(Point(position + XMFLOAT3(-scale.x / 2, -scale.y / 2, -scale.z / 2), mass / 8));

	
	rigidBody = RigidBody(points, scale, position, rotation, linearVelocity, angularMomentum);


}

XMMATRIX RigidBox::getWorldMatrix()
{
	XMMATRIX world;

	//get translation
	XMFLOAT3 pos = rigidBody.getPosition();
	XMMATRIX translationM = XMMatrixTranslation(pos.x, pos.y, pos.z);

	//get rotation
	XMMATRIX rotationM = rigidBody.getRotation();

	//get scale
	XMMATRIX scaleM = XMMatrixScaling(scale.x, scale.y, scale.z);

	world = scaleM * rotationM * translationM;

	return world;
}