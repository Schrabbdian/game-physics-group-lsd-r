//--------------------------------------------------------------------------------------
// File: main.cpp
//
// The main file containing the entry point main().
//--------------------------------------------------------------------------------------

#include <sstream>
#include <iomanip>
#include <random>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>

//DirectX includes
#include <DirectXMath.h>
using namespace DirectX;
using std::cout;

// Effect framework includes
#include <d3dx11effect.h>

// DXUT includes
#include <DXUT.h>
#include <DXUTcamera.h>

// DirectXTK includes
#include "Effects.h"
#include "VertexTypes.h"
#include "PrimitiveBatch.h"
#include "GeometricPrimitive.h"
#include "ScreenGrab.h"

// AntTweakBar includes
#include "AntTweakBar.h"

// Internal includes
#include "util/util.h"
#include "util/FFmpeg.h"

//#define TEMPLATE_DEMO
//#define MASS_SPRING_SYSTEM
//#define RIGIDBODY
//#define FLUIDSIM
#define COUPLEDSIM

#if defined(RIGIDBODY) || defined(COUPLEDSIM)
#include "RigidBody.h"
#include "RigidBox.h"
#endif

#ifdef FLUIDSIM
#include "Blob.h"
#define SEED 3
#endif

// DXUT camera
// NOTE: CModelViewerCamera does not only manage the standard view transformation/camera position 
//       (CModelViewerCamera::GetViewMatrix()), but also allows for model rotation
//       (CModelViewerCamera::GetWorldMatrix()). 
//       Look out for CModelViewerCamera::SetButtonMasks(...).
CModelViewerCamera g_camera;

// Effect corresponding to "effect.fx"
ID3DX11Effect* g_pEffect = nullptr;
ID3D11Device* g_pPd3Device = nullptr;
// Main tweak bar
TwBar* g_pTweakBar;

// DirectXTK effects, input layouts and primitive batches for different vertex types
// (for drawing multicolored & unlit primitives)
BasicEffect*                          g_pEffectPositionColor          = nullptr;
ID3D11InputLayout*                    g_pInputLayoutPositionColor     = nullptr;
PrimitiveBatch<VertexPositionColor>*  g_pPrimitiveBatchPositionColor  = nullptr;

// DirectXTK effect, input layout and primitive batch for position/normal vertices
// (for drawing unicolor & oriented & lit primitives)
BasicEffect*                          g_pEffectPositionNormal         = nullptr;
ID3D11InputLayout*                    g_pInputLayoutPositionNormal    = nullptr;
PrimitiveBatch<VertexPositionNormal>* g_pPrimitiveBatchPositionNormal = nullptr;

BasicEffect*                               g_pEffectPositionNormalColor         = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionNormalColor    = nullptr;
PrimitiveBatch<VertexPositionNormalColor>* g_pPrimitiveBatchPositionNormalColor = nullptr;

// DirectXTK simple geometric primitives
std::unique_ptr<GeometricPrimitive> g_pSphere;
std::unique_ptr<GeometricPrimitive> g_pTeapot;
std::unique_ptr<GeometricPrimitive> g_pCube;



// Movable object management
XMINT2   g_viMouseDelta = XMINT2(0,0);
XMFLOAT3 g_vfMovableObjectPos = XMFLOAT3(0,0,0);
XMFLOAT3 g_vfRotate = XMFLOAT3(0, 0, 0);

// TweakAntBar GUI variables

#ifndef COUPLEDSIM
int g_iTestCase = 3;
#else
int g_iTestCase = 0;
#endif

int g_iPreTestCase = -1;
bool  g_bSimulateByStep = false;
bool  g_bIsSpaceReleased = true;
#ifdef TEMPLATE_DEMO
int   g_iNumSpheres    = 100;
float g_fSphereSize    = 0.05f;
bool  g_bDrawTeapot    = true;
bool  g_bDrawTriangle  = true;
bool  g_bDrawSpheres = true;
#endif
#ifdef MASS_SPRING_SYSTEM
int window_width, window_height;
int indexBeingDragged = -1;


struct Spring;
struct MassPoint;
std::vector<MassPoint> g_Points;
std::vector<Spring> g_Springs;
XMFLOAT3 g_Gravity = XMFLOAT3(0.0f, -9.81f, 0.0f);
float dampingFactor = 5.0f;
#endif

#ifdef RIGIDBODY
std::vector<RigidBox> g_Boxes;
//RigidBox box1(XMFLOAT3(0.25f, 0.25f, 0), XMFLOAT3(0.15f, 0.15f, 0.15f), 2.0f, XMFLOAT3(0, 0, 0), XMFLOAT3(0, 0, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));
//RigidBox box2(XMFLOAT3(0, -0.5f, 0), XMFLOAT3(0.75f, 1.00f, 0.75f), 200.0f, XMFLOAT3(0, 0, 0), XMFLOAT3(0, 0, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));

#endif

#ifdef FLUIDSIM
std::vector<Blob> g_Blobs;
std::vector<Blob*> g_BlobsPointer;
float kernelSize = 0.03f;
float stiffness = 1;
float dampingFactor = 0.1f;
float gravityX = 0;
float gravityY = -9.81f;
float gravityZ = 0;
std::vector<int> collisionGrid; //example: 0-9 are cell1, saving the indexes of the blobs within it
#endif

#ifdef COUPLEDSIM

//RigidBody
std::vector<RigidBox> g_Boxes;

//Mass Spring
int window_width, window_height;
int indexBeingDragged = -1;

struct Spring;
struct MassPoint;
std::vector<MassPoint> g_Points;
std::vector<Spring> g_Springs;
XMFLOAT3 g_Gravity = XMFLOAT3(0.0f, -9.81f, 0.0f);
float dampingFactor = 60.0f;

//Game related
float time_step = 0.005f;
float playerAccel = 100.0f;
float maxSpeed = 3.5f;
XMFLOAT3 playingField(3.0f, 1.0f, 2.0f);
int score = 0;
int penalty = 1;
int reward = 2;


bool keys[4] = {false, false, false, false};
bool col_Wall[4] = { false, false, false, false };

int maxBoxes = 4;
float spawnTimer = 0;
float spawnInterval = 2.5f;
#endif

// Video recorder
FFmpeg* g_pFFmpegVideoRecorder = nullptr;

// Create TweakBar and add required buttons and variables
void InitTweakBar(ID3D11Device* pd3dDevice)
{
    g_pTweakBar = TwNewBar("TweakBar");
	TwDefine(" TweakBar color='0 128 128' alpha=128 ");

#ifndef COUPLEDSIM
	TwType TW_TYPE_TESTCASE = TwDefineEnumFromString("Demo", "1,2,3,4");
#else
	TwType TW_TYPE_TESTCASE = TwDefineEnumFromString("Demo", "1,2");

	TwAddVarRO(g_pTweakBar, "Score", TW_TYPE_INT32, &score, "");

#endif
	TwAddVarRW(g_pTweakBar, "Demo", TW_TYPE_TESTCASE, &g_iTestCase, "");
	// HINT: For buttons you can directly pass the callback function as a lambda expression.
	TwAddButton(g_pTweakBar, "Reset Scene", [](void *){g_iPreTestCase = -1; }, nullptr, "");
	TwAddButton(g_pTweakBar, "Reset Camera", [](void *){g_camera.Reset(); }, nullptr, "");
	// Run mode, step by step, control by space key
	TwAddVarRW(g_pTweakBar, "RunStep(space)", TW_TYPE_BOOLCPP, &g_bSimulateByStep, "");


#ifdef TEMPLATE_DEMO
	switch (g_iTestCase)
	{
	case 0:
		TwAddVarRW(g_pTweakBar, "Draw Spheres", TW_TYPE_BOOLCPP, &g_bDrawSpheres, "");
		TwAddVarRW(g_pTweakBar, "Num Spheres", TW_TYPE_INT32, &g_iNumSpheres, "min=1");
		TwAddVarRW(g_pTweakBar, "Sphere Size", TW_TYPE_FLOAT, &g_fSphereSize, "min=0.01 step=0.01");
		break;
	case 1:
		TwAddVarRW(g_pTweakBar, "Draw Teapot",   TW_TYPE_BOOLCPP, &g_bDrawTeapot, "");
		break;
	case 2:
		TwAddVarRW(g_pTweakBar, "Draw Triangle", TW_TYPE_BOOLCPP, &g_bDrawTriangle, "");
		break;
	default:
		break;
	}
#endif

#ifdef MASS_SPRING_SYSTEM
#endif

#ifdef FLUIDSIM
	TwAddVarRW(g_pTweakBar, "Gravity X", TW_TYPE_FLOAT, &gravityX, "min=-10.0 step=0.5");
	TwAddVarRW(g_pTweakBar, "Gravity Y", TW_TYPE_FLOAT, &gravityY, "min=-10.0 step=0.5");
	TwAddVarRW(g_pTweakBar, "Gravity Z", TW_TYPE_FLOAT, &gravityZ, "min=-10.0 step=0.5");
#endif

}

// Draw the edges of the bounding box [-0.5;0.5]� rotated with the cameras model tranformation.
// (Drawn as line primitives using a DirectXTK primitive batch)
void DrawBoundingBox(ID3D11DeviceContext* pd3dImmediateContext)
{
    // Setup position/color effect
    g_pEffectPositionColor->SetWorld(g_camera.GetWorldMatrix());
    
    g_pEffectPositionColor->Apply(pd3dImmediateContext);
    pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionColor);

    // Draw
    g_pPrimitiveBatchPositionColor->Begin();
    
    // Lines in x direction (red color)
    for (int i=0; i<4; i++)
    {
        g_pPrimitiveBatchPositionColor->DrawLine(
            VertexPositionColor(XMVectorSet(-0.5f, (float)(i%2)-0.5f, (float)(i/2)-0.5f, 1), Colors::Red),
            VertexPositionColor(XMVectorSet( 0.5f, (float)(i%2)-0.5f, (float)(i/2)-0.5f, 1), Colors::Red)
        );
    }

    // Lines in y direction
    for (int i=0; i<4; i++)
    {
        g_pPrimitiveBatchPositionColor->DrawLine(
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f, -0.5f, (float)(i/2)-0.5f, 1), Colors::Green),
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f,  0.5f, (float)(i/2)-0.5f, 1), Colors::Green)
        );
    }

    // Lines in z direction
    for (int i=0; i<4; i++)
    {
        g_pPrimitiveBatchPositionColor->DrawLine(
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f, (float)(i/2)-0.5f, -0.5f, 1), Colors::Blue),
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f, (float)(i/2)-0.5f,  0.5f, 1), Colors::Blue)
        );
    }

    g_pPrimitiveBatchPositionColor->End();
}

// Draw a large, square plane at y=-1 with a checkerboard pattern
// (Drawn as multiple quads, i.e. triangle strips, using a DirectXTK primitive batch)
void DrawFloor(ID3D11DeviceContext* pd3dImmediateContext)
{
    // Setup position/normal/color effect
    g_pEffectPositionNormalColor->SetWorld(XMMatrixIdentity());
    g_pEffectPositionNormalColor->SetEmissiveColor(Colors::Black);
    g_pEffectPositionNormalColor->SetDiffuseColor(0.8f * Colors::White);
    g_pEffectPositionNormalColor->SetSpecularColor(0.4f * Colors::White);
    g_pEffectPositionNormalColor->SetSpecularPower(1000);
    
    g_pEffectPositionNormalColor->Apply(pd3dImmediateContext);
    pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionNormalColor);

    // Draw 4*n*n quads spanning x = [-n;n], y = -1, z = [-n;n]
    const float n = 4;
    XMVECTOR normal      = XMVectorSet(0, 1,0,0);
    XMVECTOR planecenter = XMVectorSet(0,-1,0,0);

    g_pPrimitiveBatchPositionNormalColor->Begin();
    for (float z = -n; z < n; z++)
    {
        for (float x = -n; x < n; x++)
        {
            // Quad vertex positions
            XMVECTOR pos[] = { XMVectorSet(x  , -1, z+1, 0),
                               XMVectorSet(x+1, -1, z+1, 0),
                               XMVectorSet(x+1, -1, z  , 0),
                               XMVectorSet(x  , -1, z  , 0) };

            // Color checkerboard pattern (white & gray)
            XMVECTOR color = ((int(z + x) % 2) == 0) ? XMVectorSet(1,1,1,1) : XMVectorSet(0.6f,0.6f,0.6f,1);

            // Color attenuation based on distance to plane center
            float attenuation[] = {
                1.0f - XMVectorGetX(XMVector3Length(pos[0] - planecenter)) / n,
                1.0f - XMVectorGetX(XMVector3Length(pos[1] - planecenter)) / n,
                1.0f - XMVectorGetX(XMVector3Length(pos[2] - planecenter)) / n,
                1.0f - XMVectorGetX(XMVector3Length(pos[3] - planecenter)) / n };

            g_pPrimitiveBatchPositionNormalColor->DrawQuad(
                VertexPositionNormalColor(pos[0], normal, attenuation[0] * color),
                VertexPositionNormalColor(pos[1], normal, attenuation[1] * color),
                VertexPositionNormalColor(pos[2], normal, attenuation[2] * color),
                VertexPositionNormalColor(pos[3], normal, attenuation[3] * color)
            );
        }
    }
    g_pPrimitiveBatchPositionNormalColor->End();    
}

#ifdef TEMPLATE_DEMO
// Draw several objects randomly positioned in [-0.5f;0.5]�  using DirectXTK geometric primitives.
void DrawSomeRandomObjects(ID3D11DeviceContext* pd3dImmediateContext)
{
    // Setup position/normal effect (constant variables)
    g_pEffectPositionNormal->SetEmissiveColor(Colors::Black);
    g_pEffectPositionNormal->SetSpecularColor(0.4f * Colors::White);
    g_pEffectPositionNormal->SetSpecularPower(100);
      
    std::mt19937 eng;
    std::uniform_real_distribution<float> randCol( 0.0f, 1.0f);
    std::uniform_real_distribution<float> randPos(-0.5f, 0.5f);

    for (int i=0; i<g_iNumSpheres; i++)
    {
        // Setup position/normal effect (per object variables)
        g_pEffectPositionNormal->SetDiffuseColor(0.6f * XMColorHSVToRGB(XMVectorSet(randCol(eng), 1, 1, 0)));
        XMMATRIX scale    = XMMatrixScaling(g_fSphereSize, g_fSphereSize, g_fSphereSize);
        XMMATRIX trans    = XMMatrixTranslation(randPos(eng),randPos(eng),randPos(eng));
        g_pEffectPositionNormal->SetWorld(scale * trans * g_camera.GetWorldMatrix());

        // Draw
        // NOTE: The following generates one draw call per object, so performance will be bad for n>>1000 or so
        g_pSphere->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
    }
}

// Draw a teapot at the position g_vfMovableObjectPos.
void DrawMovableTeapot(ID3D11DeviceContext* pd3dImmediateContext)
{
    // Setup position/normal effect (constant variables)
    g_pEffectPositionNormal->SetEmissiveColor(Colors::Black);
    g_pEffectPositionNormal->SetDiffuseColor(0.6f * Colors::Cornsilk);
    g_pEffectPositionNormal->SetSpecularColor(0.4f * Colors::White);
    g_pEffectPositionNormal->SetSpecularPower(100);

    XMMATRIX scale    = XMMatrixScaling(0.5f, 0.5f, 0.5f);    
    XMMATRIX trans    = XMMatrixTranslation(g_vfMovableObjectPos.x, g_vfMovableObjectPos.y, g_vfMovableObjectPos.z);
	XMMATRIX rotations = XMMatrixRotationRollPitchYaw(g_vfRotate.x, g_vfRotate.y, g_vfRotate.z);
	g_pEffectPositionNormal->SetWorld(rotations * scale * trans);

    // Draw
    g_pTeapot->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
}

// Draw a simple triangle using custom shaders (g_pEffect)
void DrawTriangle(ID3D11DeviceContext* pd3dImmediateContext)
{
	XMMATRIX world = g_camera.GetWorldMatrix();
	XMMATRIX view  = g_camera.GetViewMatrix();
	XMMATRIX proj  = g_camera.GetProjMatrix();
	XMFLOAT4X4 mViewProj;
	XMStoreFloat4x4(&mViewProj, world * view * proj);
	g_pEffect->GetVariableByName("g_worldViewProj")->AsMatrix()->SetMatrix((float*)mViewProj.m);
	g_pEffect->GetTechniqueByIndex(0)->GetPassByIndex(0)->Apply(0, pd3dImmediateContext);
    
	pd3dImmediateContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	pd3dImmediateContext->IASetIndexBuffer(nullptr, DXGI_FORMAT_R16_UINT, 0);
	pd3dImmediateContext->IASetInputLayout(nullptr);
	pd3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pd3dImmediateContext->Draw(3, 0);
}
#endif


#if defined(MASS_SPRING_SYSTEM) || defined(COUPLEDSIM)
/*
//Operator Overloads
XMFLOAT3 operator+ (XMFLOAT3 left, XMFLOAT3 right)
{
	return XMFLOAT3(left.x + right.x, left.y + right.y, left.z + right.z);
}

XMFLOAT3 operator* (float scalar, XMFLOAT3 vector)
{
	return XMFLOAT3(scalar * vector.x, scalar * vector.y, scalar * vector.z);
}

XMFLOAT3 operator* (XMFLOAT3 vector, float scalar)
{
	return XMFLOAT3(scalar * vector.x, scalar * vector.y, scalar * vector.z);
}

std::ostream& operator<< (std::ostream& left, XMFLOAT3 right)
{
	return left << "(" << right.x << ", " << right.y << ", " << right.z << ")";
}

//helper methods
float magnitude(XMFLOAT3 vector)
{
	return sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
}

float dot(XMFLOAT3 a, XMFLOAT3 b)
{
	float result;
	result = a.x * b.x + a.y * b.y + a.z * b.z;

	return result;
}*/





//Structs

struct MassPoint
{
	XMFLOAT3 position;
	XMFLOAT3 velocity;
	float mass;
	float size = 0.025f;
	bool isFixed = false;

	XMFLOAT3 force = XMFLOAT3(0, 0, 0);
	XMFLOAT3 color = XMFLOAT3(1.0f, 0, 0);

	MassPoint(XMFLOAT3 pos, XMFLOAT3 vel, float m)
	{
		position = pos;
		velocity = vel;
		mass = m;
	}
};

struct Spring
{
	//indices in an array/vector of all mass points
	int start;
	int end;

	float restingLength;
	float stiffness;

	Spring(int s, int e, float length, float stiff)
	{
		start = s;
		end = e;
		restingLength = length;
		stiffness = stiff;
	}

	//Hooke's law (returns force at start point)
	void applyForce(std::vector<MassPoint>& points)
	{
		XMFLOAT3 connectionVector = (points[start].position + -1.0f * points[end].position);
		float l = magnitude(connectionVector);

		XMFLOAT3 resultingForce = -stiffness * (l - restingLength) * connectionVector * (1 / l);
		
		//apply force to start and end points
		points[start].force = points[start].force + resultingForce;
		points[end].force = points[end].force + -1.0f * resultingForce;
	}
};


//Collision methods

//returns -1 if nothing was hit, otherwise the index of the first MassPoint hit
int rayCast(XMFLOAT3 origin, XMFLOAT3 direction, XMFLOAT3& nearestPos)
{
	//normalize direction
	direction = direction * (1.0f / magnitude(direction));

	float shortestDistance = D3D11_FLOAT32_MAX;
	int result = -1;

	//check for all points whether they collide with the ray
	for (int i = 0; i < g_Points.size(); i++)
	{
		XMFLOAT3 toPoint = g_Points[i].position + -1.0f * origin;//vector from origin to masspoint

		//project toPoint onto direction
		XMFLOAT3 projection = dot(toPoint, direction) * direction;
		nearestPos = (projection + origin);


		float distanceFromRay = magnitude(-1.0f * (toPoint + origin) + nearestPos);//shortest distance between MassPoint and Ray
		float distanceToCamera = magnitude(toPoint);


		//if the point is close enough to the ray
		if (distanceFromRay <= g_Points[i].size)
		{
			if (shortestDistance > distanceToCamera)
			{
				shortestDistance = distanceToCamera;
				result = i;
			}
		}

	}

	return result;

}




//Force calculation methods
void calculateSpringForces(std::vector<MassPoint>& points, std::vector<Spring>& springs)
{
	//clear force value of points
	for (int i = 0; i < points.size(); i++)
	{
		points[i].force = XMFLOAT3(0, 0, 0);
	}

	//calculate spring forces
	for (int j = 0; j < springs.size(); j++)
	{
		springs[j].applyForce(points);
	}
}


void calculateSpringForcesGravity(std::vector<MassPoint>& points, std::vector<Spring>& springs)
{
	//add spring forces
	calculateSpringForces(points, springs);

	//add gravity
	for (int i = 0; i < points.size(); i++)
	{
		points[i].force = points[i].force + g_Gravity;
	}
}

void calculateSpringForcesGravityDamping(std::vector<MassPoint>& points, std::vector<Spring>& springs)
{
	//add spring forces
	calculateSpringForces(points, springs);
	
	for (int i = 0; i < points.size(); i++)
	{
		points[i].force = points[i].force + g_Gravity;//add gravity
		points[i].force = points[i].force - points[i].velocity * dampingFactor;//add damping
	}
}


//Integration Methods

//Euler (requires a pointer to a function which calculates the forces for all points)
void eulerStep(std::vector<MassPoint>& points, std::vector<Spring>& springs, 
					void(*calculateForces) (std::vector<MassPoint>&, std::vector<Spring>&), float timeStep)
{
	//calculate forces for all points
	calculateForces(points, springs);

	for (int i = 0; i < points.size(); i++)
	{
		if (!points[i].isFixed)
		{

			XMFLOAT3 newPosition = points[i].position + points[i].velocity * timeStep;
			XMFLOAT3 newVelocity = points[i].velocity + (points[i].force * (1.0f / points[i].mass)) * timeStep;

			points[i].position = newPosition;
			points[i].velocity = newVelocity;
		}
	}
}


//Midpoint (requires a pointer to a function which calculates the forces for all points)
void midpointStep(std::vector<MassPoint>& points, std::vector<Spring>& springs, 
					void(*calculateForces) (std::vector<MassPoint>&, std::vector<Spring>&), float timeStep)
{
	std::vector<XMFLOAT3> originalPositions;
	std::vector<XMFLOAT3> originalVelocities;
	
	//calculate original forces
	calculateForces(points, springs);



	//calculate midpoint step for all points
	for (int i = 0; i < points.size(); i++)
	{
		//save original position / velocity for later use
		XMFLOAT3 originalPos = points[i].position;
		XMFLOAT3 originalVel = points[i].velocity;

		originalPositions.push_back(originalPos);
		originalVelocities.push_back(originalVel);

		if (!points[i].isFixed) 
		{
			//calculate position / velocity at midpoint
			points[i].position = originalPos + originalVel * (timeStep / 2.0f);
			points[i].velocity = originalVel + points[i].force * (1.0f / points[i].mass) * (timeStep / 2.0f);
		}
	}




	//--> Midpoint results are now stored in the point

	//recalculate forces based on midpoint results
	calculateForces(points, springs);




	//calculate full step for all points
	for (int i = 0; i < points.size(); i++)
	{
		if (!points[i].isFixed) 
		{

			//calculate final velocity / position
			points[i].position = originalPositions[i] + points[i].velocity * timeStep;
			points[i].velocity = originalVelocities[i] + points[i].force * (1.0f / points[i].mass) * timeStep;
		}
	}

}
#endif

#ifdef MASS_SPRING_SYSTEM
//Update Methods
void UpdateDemo2(float deltaTime)
{
	eulerStep(g_Points, g_Springs, calculateSpringForces, 0.005f);
}

void UpdateDemo3(float deltaTime)
{
	midpointStep(g_Points, g_Springs, calculateSpringForces, 0.005f);
}

void UpdateDemo4(float deltaTime)
{
	
	for (int i = 0; i < g_Points.size(); i++)
	{
		//collision with ground floor
		if (g_Points[i].position.y < -0.5f)
		{
			g_Points[i].position.y = -0.5f;
			g_Points[i].velocity.y = 0.0f;
			g_Points[i].force.y = 0.0f;
		}

		//collision with walls
		if (g_Points[i].position.x < -0.5f)
		{
			g_Points[i].position.x = -0.5f;
			g_Points[i].velocity.x = 0.0f;
			g_Points[i].force.x = 0.0f;
		}

		if (g_Points[i].position.x > 0.5f)
		{
			g_Points[i].position.x = 0.5f;
			g_Points[i].velocity.x = 0.0f;
			g_Points[i].force.x = 0.0f;
		}

		if (g_Points[i].position.z < -0.5f)
		{
			g_Points[i].position.z = -0.5f;
			g_Points[i].velocity.z = 0.0f;
			g_Points[i].force.z = 0.0f;
		}

		if (g_Points[i].position.z > 0.5f)
		{
			g_Points[i].position.z = 0.5f;
			g_Points[i].velocity.z = 0.0f;
			g_Points[i].force.z = 0.0f;
		}
	}

	//midpointStep(g_Points, g_Springs, calculateSpringForcesGravity, deltaTime);
	//eulerStep(g_Points, g_Springs, calculateSpringForces, deltaTime);
	midpointStep(g_Points, g_Springs, calculateSpringForcesGravityDamping, deltaTime);
	


}

#endif

#if defined(FLUIDSIM) || defined(MASS_SPRING_SYSTEM) || defined(COUPLEDSIM)	
//Drawing methods
void DrawSphere(ID3D11DeviceContext* context, XMFLOAT3 position, float radius, XMFLOAT3 color)
{
	// Set attributes in shader
	g_pEffectPositionNormal->SetDiffuseColor(XMVectorSet(color.x, color.y, color.z, 1));//color
	XMMATRIX scale = XMMatrixScaling(radius, radius, radius);//radius
	XMMATRIX translation = XMMatrixTranslation(position.x, position.y, position.z);//position

	g_pEffectPositionNormal->SetWorld(scale * translation * g_camera.GetWorldMatrix());

	// actually draw the sphere
	g_pSphere->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
}
#endif

#if defined(MASS_SPRING_SYSTEM) || defined(COUPLEDSIM)
//Drawing Methods

//Only call this if "g_pPrimitiveBatchPositionColor->Begin();" has been called before
void DrawLine(ID3D11DeviceContext* context, XMFLOAT3 start, XMFLOAT3 end, XMFLOAT3 color)
{
	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor(start, XMFLOAT4(color.x, color.y, color.z, 1.0f)),
		VertexPositionColor(end, XMFLOAT4(color.x, color.y, color.z, 1.0f))
		);
}

void DrawMassSpringSystem(ID3D11DeviceContext* context)
{
	// Setup variables that are the same for all mass points
	g_pEffectPositionNormal->SetEmissiveColor(Colors::Black);
	g_pEffectPositionNormal->SetSpecularColor(0.4f * Colors::White);
	g_pEffectPositionNormal->SetSpecularPower(100);

	//Draw all mass points (spheres) here
	//DrawSphere(context, XMFLOAT3(0, 0.5f, 0), 0.5f, XMFLOAT3(1.0f, 0.0f, 0.0f));
	//DrawSphere(context, XMFLOAT3(0, -0.5f, 0), 0.5f, XMFLOAT3(1.0f, 0.0f, 0.0f));

	for (std::vector<MassPoint>::iterator it = g_Points.begin(); it != g_Points.end(); it++)
	{
		DrawSphere(context, it->position, it->size, it->color);
	}


	// Setup Line drawing
	g_pEffectPositionColor->SetWorld(g_camera.GetWorldMatrix());

	g_pEffectPositionColor->Apply(context);
	context->IASetInputLayout(g_pInputLayoutPositionColor);

	g_pPrimitiveBatchPositionColor->Begin();

	//Draw all connections (lines) here
	//DrawLine(context, XMFLOAT3(0, 0.5f, 0), XMFLOAT3(0, -0.5f, 0), XMFLOAT3(1.0, 0, 0));

	for (std::vector<Spring>::iterator it = g_Springs.begin(); it != g_Springs.end(); it++)
	{
		XMFLOAT3 startPos, endPos;
		startPos = g_Points[it->start].position;
		endPos = g_Points[it->end].position;
		DrawLine(context, startPos, endPos, g_Points[it->start].color);
	}

	g_pPrimitiveBatchPositionColor->End();

}


//Helper Methods
void createCube(XMFLOAT3 position, float size, float stiffness, float mass)
{
	int g_PointsSize = g_Points.size();
	std::vector<MassPoint> cubePoints;

	cubePoints.push_back(MassPoint(XMFLOAT3(0, 0, 0), XMFLOAT3(0, 0, 0), mass / 8.0f));
	cubePoints.push_back(MassPoint(XMFLOAT3(1, 0, 0), XMFLOAT3(0, 0, 0), mass / 8.0f));
	cubePoints.push_back(MassPoint(XMFLOAT3(0, 0, 1), XMFLOAT3(0, 0, 0), mass / 8.0f));
	cubePoints.push_back(MassPoint(XMFLOAT3(1, 0, 1), XMFLOAT3(0, 0, 0), mass / 8.0f));

	cubePoints.push_back(MassPoint(XMFLOAT3(0, 1, 0), XMFLOAT3(0, 0, 0), mass / 8.0f));
	cubePoints.push_back(MassPoint(XMFLOAT3(1, 1, 0), XMFLOAT3(0, 0, 0), mass / 8.0f));
	cubePoints.push_back(MassPoint(XMFLOAT3(0, 1, 1), XMFLOAT3(0, 0, 0), mass / 8.0f));
	cubePoints.push_back(MassPoint(XMFLOAT3(1, 1, 1), XMFLOAT3(0, 0, 0), mass / 8.0f));


	//scale and move cube
	for (int i = 0; i < cubePoints.size(); i++)
	{
		cubePoints[i].position = (cubePoints[i].position * size) + position;
	}

	//add cube to g_Points
	for (int i = 0; i < cubePoints.size(); i++)
	{
		g_Points.push_back(cubePoints[i]);
	}

	//create springs between all points
	for (int i = 0; i < cubePoints.size(); i++)
	{
		for (int j = cubePoints.size() - 1; j > i; j--)
		{
			float distance = magnitude(cubePoints[i].position + -1.0f * cubePoints[j].position);
			g_Springs.push_back(Spring(j + g_PointsSize, i + g_PointsSize, distance, stiffness));
		}
	}
}

#define IDX(x, y, z, size_x, size_y) ((x) + (size_x) * (y) + (size_x) * (size_y) * (z))

void createGrid(XMFLOAT3 midPoint, XMFLOAT3 size, XMFLOAT3 color, float stiffness, float mass, int resolution)
{
	int g_PointsSize = g_Points.size();
	std::vector<MassPoint> gridPoints;

	float x_spacing = size.x / (float)(resolution - 1);
	float y_spacing = size.y / (float)(resolution - 1);
	float z_spacing = size.z / (float)(resolution - 1);

	//create a grid
	for (int z = 0; z < resolution; z++)
	{
		for (int y = 0; y < resolution; y++)
		{
			for (int x = 0; x < resolution; x++)
			{
				MassPoint p(midPoint + XMFLOAT3(x * x_spacing, y * y_spacing, z * z_spacing), XMFLOAT3(0, 0, 0), mass / (float)pow(resolution, 3));
				gridPoints.push_back(p);
			}
		}
	}

	//move grid
	for (int i = 0; i < gridPoints.size(); i++)
	{
		gridPoints[i].position.x -= 0.5f * size.x;
		gridPoints[i].position.y -= 0.5f * size.y;
		gridPoints[i].position.z -= 0.5f * size.z;

		gridPoints[i].position = gridPoints[i].position + midPoint;
	}

	//add grid to g_Points
	for (int i = 0; i < gridPoints.size(); i++)
	{
		g_Points.push_back(gridPoints[i]);
	}

	//create a spring between every point and its neighbours
	for (int z = 0; z < resolution; z++)
	{
		for (int y = 0; y < resolution; y++)
		{
			for (int x = 0; x < resolution; x++)
			{
				g_Points[IDX(x, y, z, resolution, resolution) + g_PointsSize].color = color;

				if (x < resolution - 1)
				{
					//horizontal Springs
					float distX = magnitude(g_Points[IDX(x, y, z, resolution, resolution) + g_PointsSize].position - g_Points[IDX(x + 1, y, z, resolution, resolution) + g_PointsSize].position);
					Spring s(IDX(x, y, z, resolution, resolution) + g_PointsSize, IDX(x + 1, y, z, resolution, resolution) + g_PointsSize, distX, stiffness);
					g_Springs.push_back(s);
				}

				if (z < resolution - 1)
				{
					//depth Springs
					float distZ = magnitude(g_Points[IDX(x, y, z, resolution, resolution) + g_PointsSize].position - g_Points[IDX(x, y, z + 1, resolution, resolution) + g_PointsSize].position);
					Spring s(IDX(x, y, z, resolution, resolution) + g_PointsSize, IDX(x, y, z + 1, resolution, resolution) + g_PointsSize, distZ, stiffness);
					g_Springs.push_back(s);
				}

				if (x < resolution - 1 && z < resolution - 1)
				{
					//diagonal Springs
					float distDiag = magnitude(g_Points[IDX(x, y, z, resolution, resolution) + g_PointsSize].position - g_Points[IDX(x + 1, y, z + 1, resolution, resolution) + g_PointsSize].position);
					Spring s(IDX(x, y, z, resolution, resolution) + g_PointsSize, IDX(x + 1, y, z + 1, resolution, resolution) + g_PointsSize, distDiag, stiffness);
					g_Springs.push_back(s);
					s = Spring(IDX(x, y, z + 1, resolution, resolution) + g_PointsSize, IDX(x + 1, y, z, resolution, resolution) + g_PointsSize, distDiag, stiffness);
					g_Springs.push_back(s);
				}

				if (y < resolution - 1)
				{
					//vertical Springs
					float distY = magnitude(g_Points[IDX(x, y, z, resolution, resolution) + g_PointsSize].position - g_Points[IDX(x, y + 1, z, resolution, resolution) + g_PointsSize].position);
					Spring s(IDX(x, y, z, resolution, resolution) + g_PointsSize, IDX(x, y + 1, z, resolution, resolution) + g_PointsSize, distY, stiffness);
					g_Springs.push_back(s);
				}

				if (x < resolution - 1 && y < resolution - 1 && z < resolution - 1)
				{
					//vertical diagonal Springs
					float distDiag = magnitude(g_Points[IDX(x, y, z, resolution, resolution) + g_PointsSize].position - g_Points[IDX(x + 1, y + 1, z + 1, resolution, resolution) + g_PointsSize].position);
					Spring s(IDX(x, y, z, resolution, resolution) + g_PointsSize, IDX(x + 1, y + 1, z + 1, resolution, resolution) + g_PointsSize, distDiag, stiffness);
					g_Springs.push_back(s);
					s = Spring(IDX(x, y + 1, z, resolution, resolution) + g_PointsSize, IDX(x + 1, y, z + 1, resolution, resolution) + g_PointsSize, distDiag, stiffness);
					g_Springs.push_back(s);
					s = Spring(IDX(x, y, z + 1, resolution, resolution) + g_PointsSize, IDX(x + 1, y + 1, z, resolution, resolution) + g_PointsSize, distDiag, stiffness);
					g_Springs.push_back(s);
					s = Spring(IDX(x, y + 1, z + 1, resolution, resolution) + g_PointsSize, IDX(x + 1, y, z, resolution, resolution) + g_PointsSize, distDiag, stiffness);
					g_Springs.push_back(s);
				}
			}
		}
	}
	

	

}

#endif

#if defined(RIGIDBODY) || defined(COUPLEDSIM)
bool calculateCollision(RigidBox* box1, RigidBox* box2)
{
	CollisionInfo colResult = checkCollision(box1->getWorldMatrix(), box2->getWorldMatrix());

	//if a collision occurred
	if (colResult.isValid)
	{
		//NOT REALLY TESTED
		XMFLOAT3 normal, colPoint;
		XMStoreFloat3(&normal, colResult.normalWorld);
		XMStoreFloat3(&colPoint, colResult.collisionPointWorld);

		//collision response for (B, A)
		float impulse = RigidBody::calculateImpulse(box1->rigidBody, box2->rigidBody, -1.0f * normal, colPoint, 0.0f);


		box1->rigidBody.applyImpulseAtPoint(colPoint, normal, impulse);
		box2->rigidBody.applyImpulseAtPoint(colPoint, normal, -impulse);

		return true;
	}
	else
	{
		//check pair of boxes the other way around
		colResult = checkCollision(box2->getWorldMatrix(), box1->getWorldMatrix());

		if (colResult.isValid)
		{

			XMFLOAT3 normal, colPoint;
			XMStoreFloat3(&normal, colResult.normalWorld);
			XMStoreFloat3(&colPoint, colResult.collisionPointWorld);

			//collision response for (B, A)
			float impulse = RigidBody::calculateImpulse(box2->rigidBody, box1->rigidBody, -1.0f * normal, colPoint, 0.0f);


			box2->rigidBody.applyImpulseAtPoint(colPoint, normal, impulse);
			box1->rigidBody.applyImpulseAtPoint(colPoint, normal, -impulse);
			return true;
		}
	}

	return false;
}

void calculateCollisionForAll()
{
	for (int i = 0; i < g_Boxes.size(); i++)
	{
		for (int j = g_Boxes.size() - 1; j != i && j > 0; j--)
		{
			bool collided = calculateCollision(&g_Boxes[i], &g_Boxes[j]);

#ifdef COUPLEDSIM
			if (collided && i == 0)//a Box fell on the ground
			{
				g_Boxes[j].color = XMFLOAT3(1.0f, 0.0f, 0.0f);
				
			}
#endif
		}
	}
}
#endif

#ifdef COUPLEDSIM
float calculateImpulse(RigidBody a, MassPoint b, XMFLOAT3 collisionNormal, XMFLOAT3 collisionPointWorld, float c)//this function is ugly, don't look at it
{
	//called on collision of point b with object a

	//get required variables
	XMVECTOR colPoint_a = XMLoadFloat3(&(collisionPointWorld - a.getPosition()));
	XMVECTOR colPoint_b = XMLoadFloat3(&(collisionPointWorld - b.position));
	XMVECTOR n = XMLoadFloat3(&collisionNormal);
	XMMATRIX I_inv_a = XMLoadFloat3x3(&a.getInverseInertiaTensor());


	//calculate relative velocity
	XMFLOAT3 contactVelocity_a = a.getLinearVelocity() + cross(collisionPointWorld - a.getPosition(), a.getAngularVelocity());
	XMFLOAT3 contactVelocity_b = b.velocity;

	XMFLOAT3 relativeVelocity = (contactVelocity_b - contactVelocity_a);

	if (dot(relativeVelocity, collisionNormal) <= 0)
	{
		//calculate "angular impulses"
		XMVECTOR angularImpulse_a = XMVector3Transform(XMVector3Cross(colPoint_a, n), I_inv_a);


		//calculate numerator
		float numerator = dot(-(1 + c) * relativeVelocity, collisionNormal);

		//calculate denominator
		XMFLOAT3 temp;
		XMStoreFloat3(&temp, XMVector3Cross(angularImpulse_a, colPoint_a));

		float denominator = a.getInverseMass() + (1.0f / b.mass) + dot(temp, collisionNormal);

		//calculate final impulse
		float J = numerator / denominator;

		return J;
	}
	else return 0.0f;
}

bool calculateCollision(RigidBox* box, MassPoint* point)
{
	XMFLOAT3 worldPoint = point->position;
	CollisionInfo colResult = checkCollision(box->getWorldMatrix(), XMVectorSet(worldPoint.x, worldPoint.y, worldPoint.z, 1));

	//if a collision occurred
	if (colResult.isValid)
	{
		XMFLOAT3 normal, colPoint;
		XMStoreFloat3(&normal, colResult.normalWorld);
		XMStoreFloat3(&colPoint, colResult.collisionPointWorld);

		//collision response for (B, A)
		float impulse = calculateImpulse(box->rigidBody, *point, -1.0f * normal, colPoint, 0.0f);


		box->rigidBody.applyImpulseAtPoint(colPoint, normal, impulse);
		point->velocity = point->velocity - impulse * normal * (1.0f / point->mass);
		
		point->position = point->position - normal * colResult.colDist;//actually PREVENT collision

		return true;
	}

	return false;
}
#endif

#ifdef RIGIDBODY
void UpdateRigidDemo(float deltaTime)
{
	calculateCollisionForAll();
	for (int i = 0; i < g_Boxes.size(); i++)
	{
		g_Boxes[i].update(deltaTime);
	}
}

void UpdateRigidDemo4(float deltaTime)
{
	calculateCollisionForAll(); 
	for (int i = 0; i < g_Boxes.size(); i++)
	{
		g_Boxes[i].rigidBody.applyForceAtPoint(XMFLOAT3(0, -1.0f, 0), g_Boxes[i].rigidBody.getPosition());//apply gravity
		g_Boxes[i].update(deltaTime);
	}
}

#endif

#ifdef FLUIDSIM	
void DrawFluid(ID3D11DeviceContext* context)
{
	// Setup variables that are the same for all mass points
	g_pEffectPositionNormal->SetEmissiveColor(Colors::Black);
	g_pEffectPositionNormal->SetSpecularColor(0.4f * Colors::White);
	g_pEffectPositionNormal->SetSpecularPower(100);

	for (std::vector<Blob>::iterator it = g_Blobs.begin(); it != g_Blobs.end(); it++)
	{
		DrawSphere(context, it->position, it->radius, XMFLOAT3(0,0,1));
	}
}


//Physical simulation methods
void eulerStep(float timeStep)
{
	//euler step for each blob
	for (int i = 0; i < g_Blobs.size(); i++)
	{
		Vector3 newPosition = g_Blobs[i].position + g_Blobs[i].velocity * timeStep;
		Vector3 newVelocity = g_Blobs[i].velocity + timeStep * (g_Blobs[i].force * (1.0f/g_Blobs[i].mass));
		//Vector3 newVelocity = g_Blobs[i].velocity + timeStep * (g_Blobs[i].force * (1.0f / g_Blobs[i].density));

		g_Blobs[i].position = newPosition;
		g_Blobs[i].velocity = newVelocity;

		g_Blobs[i].force = Vector3(0, 0, 0);//clear force since it has been "used up"
	}

}

void constrainBlobsToBox()
{
	float bounciness = 0.3f;

	for (int i = 0; i < g_Blobs.size(); i++)
	{
		if (g_Blobs[i].position.x > 0.5f) { g_Blobs[i].position.x = 0.5f; g_Blobs[i].velocity.x *= -bounciness; }
		if (g_Blobs[i].position.x < -0.5f) { g_Blobs[i].position.x = -0.5f; g_Blobs[i].velocity.x *= -bounciness; }

		if (g_Blobs[i].position.y > 0.5f) { g_Blobs[i].position.y = 0.5f; g_Blobs[i].velocity.y *= -bounciness; }
		if (g_Blobs[i].position.y < -0.5f) { g_Blobs[i].position.y = -0.5f; g_Blobs[i].velocity.y *= -bounciness; }

		if (g_Blobs[i].position.z > 0.5f) { g_Blobs[i].position.z = 0.5f; g_Blobs[i].velocity.z *= -bounciness; }
		if (g_Blobs[i].position.z < -0.5f) { g_Blobs[i].position.z = -0.5f; g_Blobs[i].velocity.z *= -bounciness; }
	}
}

void createCollisionGrid(int gridSideCellCount, int maxBlobsPerCell)
{
	//create grid 'array' (this is ugly, pls refactor)
	for (int i = 0; i < (pow(gridSideCellCount, 3) * maxBlobsPerCell); i++)
	{
		collisionGrid.push_back(-1); //maxBlobsPerCell-times for every cell
	}
}


/* takes GRID coordinates, returns index of cell */
int getCellAtCoordinates(int gridSideCellCount, Vector3 coordinates)
{
	return coordinates.x + (coordinates.y * gridSideCellCount) + (coordinates.z * pow(gridSideCellCount, 2));
}

//collisiongrid functions
/* takes WORLD coordinates, returns index of cell*/
int getCellAtPosition(int gridSideCellCount, float cellLength, Vector3 position)
{
	//project point in a uniscale grid starting at 0,0,0
	float gridSideLength = gridSideCellCount * cellLength;
	Vector3 gridOffset = Vector3(-gridSideLength / 2.0f, gridSideLength / 2.0f, gridSideLength / 2.0f);

	position -= gridOffset;
	position /= cellLength;

	//floor positions
	position = Vector3(floorf(position.x), floorf(-position.y), floorf(-position.z));

	//TODO: What, if position is not in grid?

	int result = getCellAtCoordinates(gridSideCellCount, position);

	//get cell and return
	return result;
}

Vector3 getCellPositionFromIndex(int cellIndex, int gridSideCellCount)
{
	//cellIndex -> cellPosition
	int cellX = cellIndex % gridSideCellCount;
	int cellY = (cellIndex % (int)pow(gridSideCellCount, 2) - cellX) / gridSideCellCount;
	int cellZ = (cellIndex - cellX - cellY) / pow(gridSideCellCount, 2);
	return Vector3(cellX, cellY, cellZ);
}

/* Finds neighbours for all blobs in g_blobs and writes them in their neighbour-field. */
void findBlobNeighbours(float gridSideLength, float cellLength, int maxBlobsPerCell)
{
	//--create grid
	//calculate grid length in cells (gets bigger, if values don't fit)
	int gridSideCellCount = (int)ceilf(gridSideLength / cellLength);

	//create grid 'array' and counter for blobs in cells (this is ugly, pls refactor)
	if (collisionGrid.empty()) { createCollisionGrid(gridSideCellCount, maxBlobsPerCell); }

	std::vector<int> collisionGridCellCounter;
	for (int i = 0; i < pow(gridSideCellCount, 3); i++)
	{
		collisionGridCellCounter.push_back(0); //for every cell
	}

	//--sort blobs into grid
	for (int i = 0; i < g_Blobs.size(); i++)
	{
		//get cell index at position
		int cellIndex = getCellAtPosition(gridSideCellCount, cellLength, g_Blobs[i].position);
		//set blob in grid array
		collisionGrid[(cellIndex * maxBlobsPerCell) + collisionGridCellCounter[cellIndex]] = i;
		collisionGridCellCounter[cellIndex] += 1;
		//TODO: What if there are already more blobs in the cell than maxBlobPerCell? (ignore)
	}

	//--write neighbours for every blob in blob (including themselves)
	//go over all cells
	for (int cellNo = 0; cellNo < pow(gridSideCellCount, 3); cellNo++)
	{
		//Don't do anything if cell is empty
		if (collisionGridCellCounter[cellNo] > 0)
		{
			//cellIndex -> cellPosition
			Vector3 cellPosition = getCellPositionFromIndex(cellNo, gridSideCellCount);

			//find neighbours (already include themselves)
			std::vector<Blob*> neighbours;
			for (int x = -1; x < 2; x++) 		//go over neighbour cells
			{
				for (int y = -1; y < 2; y++)
				{
					for (int z = -1; z < 2; z++)
					{
						Vector3 neighbourCellPos = Vector3(x + cellPosition.x, y + cellPosition.y, z + cellPosition.z);
						int neighbourCellNo = getCellAtCoordinates(gridSideCellCount,  neighbourCellPos);
						//if cell is in grid
						if ((   x + cellPosition.x) > 0 || (x + cellPosition.x) < (gridSideCellCount - 1)
							|| (y + cellPosition.y) > 0 || (y + cellPosition.y) < (gridSideCellCount - 1)
							|| (z + cellPosition.z) > 0 || (z + cellPosition.z) < (gridSideCellCount - 1))
						{
							//add each blob in it to neighbour list
							for (int cellBlob_i = 0; cellBlob_i < collisionGridCellCounter[neighbourCellNo]; cellBlob_i++)
							{
								neighbours.push_back(&g_Blobs[collisionGrid[(neighbourCellNo * maxBlobsPerCell) + cellBlob_i]]);
							}
						}
					}
				}
			}

			//add neighbours to every blob in cell
			for (int thisCellBlob_i = 0; thisCellBlob_i < collisionGridCellCounter[cellNo]; thisCellBlob_i++)
			{
				int currentBlobIndex = collisionGrid[(cellNo * maxBlobsPerCell) + thisCellBlob_i];
				for (int i = 0; i < neighbours.size(); i++)
				{
					g_Blobs[currentBlobIndex].neighbours.push_back(neighbours[i]);
				}
			}
		}
	}
}

//create cube method
void createBlobCube(int blobCubeSideCount)
{
	g_Blobs.clear();

	//std::srand(unsigned(time(0)));
	std::srand(SEED);

	float blobDistance = 2 * kernelSize;
	float blobCubeSideLength = blobDistance * blobCubeSideCount;
	Vector3 blobCubeOffset = Vector3(-blobCubeSideLength / 2.0f, -blobCubeSideLength / 2.0f, -blobCubeSideLength / 2.0f);

	for (int x = 0; x < blobCubeSideCount; x++)
	{
		for (int y = 0; y < blobCubeSideCount; y++)
		{
			for (int z = 0; z < blobCubeSideCount; z++)
			{
				float randomValue1 = (float)std::rand() / (float)RAND_MAX * 0.1f * kernelSize;
				float randomValue2 = (float)std::rand() / (float)RAND_MAX * 0.1f * kernelSize;
				float randomValue3 = (float)std::rand() / (float)RAND_MAX * 0.1f * kernelSize;
				Blob blobX(Vector3(x * blobDistance + randomValue1, y * blobDistance + randomValue2, z * blobDistance + randomValue3) + blobCubeOffset);
				blobX.id = z + y * blobCubeSideCount + x * pow(blobCubeSideCount, 2);
				g_Blobs.push_back(blobX);
			}
		}
	}
	/*
	Blob blobX(Vector3(0, 0, 0));
	Blob blobY(Vector3(0, 0, 0.03f));
	g_Blobs.push_back(blobX);
	g_Blobs.push_back(blobY);
	*/
}

//Update methods

void UpdateFluidDemo1(float timeStep)
{
	Vector3 gravity = Vector3(gravityX, gravityY, gravityZ);

	findBlobNeighbours(1.1f, kernelSize * 2, 10);

	for (int i = 0; i < g_Blobs.size(); i++)
	{
		g_Blobs[i].calculateDensity(g_Blobs[i].neighbours, kernelSize);
		g_Blobs[i].calculatePressure(stiffness);
	}

	for (int i = 0; i < g_Blobs.size(); i++)
	{
		g_Blobs[i].calculatePressureForce(g_Blobs[i].neighbours, kernelSize, -1);
	}

	//add gravity
	for (int i = 0; i < g_Blobs.size(); i++)
	{
		g_Blobs[i].force += gravity * g_Blobs[i].mass;
	}

	//add damping
	for (int i = 0; i < g_Blobs.size(); i++)
	{
		g_Blobs[i].force -= g_Blobs[i].velocity * dampingFactor;
	}

	eulerStep(timeStep);
	constrainBlobsToBox();

	//remove blob neighbours
	for (int i = 0; i < g_Blobs.size(); i++)
	{
		g_Blobs[i].neighbours.clear();
	}

}

void UpdateFluidDemo2(float timeStep)
{
	Vector3 gravity = Vector3(gravityX, gravityY, gravityZ);

	for (int i = 0; i < g_Blobs.size(); i++)
	{
		g_Blobs[i].calculateDensity(g_BlobsPointer, kernelSize);
		g_Blobs[i].calculatePressure(stiffness);
	}

	for (int i = 0; i < g_Blobs.size(); i++)
	{
		g_Blobs[i].calculatePressureForce(g_BlobsPointer, kernelSize, i);
	}

	//add gravity
	for (int i = 0; i < g_Blobs.size(); i++)
	{
		g_Blobs[i].force += gravity * g_Blobs[i].mass;
	}

	//add damping
	for (int i = 0; i < g_Blobs.size(); i++)
	{
		g_Blobs[i].force -= g_Blobs[i].velocity * dampingFactor;
	}

	eulerStep(timeStep);
	constrainBlobsToBox();
}

void UpdateFluidDemo3(float timeStep)
{
}

void UpdateFluidDemo4(float timeStep)
{
}
#endif

#ifdef COUPLEDSIM

void calculateCollisionForAll_coupled()
{
	for (int i = 0; i < g_Boxes.size(); i++)
	{
		for (int j = 0; j < g_Points.size(); j++)
		{
			calculateCollision(&g_Boxes[i], &g_Points[j]);
		}
	}
}

void removeOutOfBoundsBoxes()
{
	//first box is always ground so start at 1 instead of 0
	for (int i = 1; i < g_Boxes.size(); )
	{
		XMFLOAT3 pos = g_Boxes[i].rigidBody.getPosition();
		if (pos.y < -1.5f || pos.x > 5.0f || pos.x < -5.0f || pos.z > 5.0f || pos.z < -5.0f)
		{
			//if the Box did not touch the ground before, add score
			if (g_Boxes[i].color != XMFLOAT3(1.0f, 0.0f, 0.0f))
			{
				score += reward;
			}
			else
			{
				score -= penalty;
			}

			//erase box
			g_Boxes.erase(g_Boxes.begin() + i);
		}
		else
		{
			i++;
		}
	}
}

void handleSpawning(float timeStep)
{
	spawnTimer += timeStep;

	//if the spawntimer ticks and there is room for new boxes
	if (spawnTimer > spawnInterval && g_Boxes.size() < maxBoxes)
	{
		spawnTimer = 0;//reset timer

		//calculate random spawn position above playing field
		float x = ((float)rand() / (float)RAND_MAX) * playingField.x - 0.5f * playingField.x;
		float z = ((float)rand() / (float)RAND_MAX) * playingField.z - 0.5f * playingField.z;

		//spawn new box
		RigidBox box(XMFLOAT3(x, 2.5f, z), XMFLOAT3(0.25f, 0.25f, 0.25f), 100.0f, XMFLOAT3(0.1f, 0, 0.1f), XMFLOAT3(0, -1.0f, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));
		box.color = XMFLOAT3(0.2f, 1.0f, 0.2f);
		g_Boxes.push_back(box);
	}
}

void UpdateCoupledDemo1(float timeStep)
{
	handleSpawning(timeStep);

	//user input
	for (int i = 0; i < g_Points.size(); i++)
	{
		if (keys[0] && !col_Wall[0]) { g_Points[i].velocity.z += timeStep * playerAccel; }
		if (keys[1] && !col_Wall[1]) { g_Points[i].velocity.z -= timeStep * playerAccel; }
		if (keys[2] && !col_Wall[2]) { g_Points[i].velocity.x -= timeStep * playerAccel; }
		if (keys[3] && !col_Wall[3]) { g_Points[i].velocity.x += timeStep * playerAccel; }
	}

	//limit player velocity
	for (int i = 0; i < g_Points.size(); i++)
	{
		if (g_Points[i].velocity.z > maxSpeed) { g_Points[i].velocity.z = maxSpeed; }
		if (g_Points[i].velocity.z < -maxSpeed) { g_Points[i].velocity.z = -maxSpeed; }
		if (g_Points[i].velocity.x > maxSpeed) { g_Points[i].velocity.x = maxSpeed; }
		if (g_Points[i].velocity.x < -maxSpeed) { g_Points[i].velocity.x = -maxSpeed; }
	}

	//reset wall collisions
	col_Wall[0] = false; col_Wall[1] = false; col_Wall[2] = false; col_Wall[3] = false;

	//Collisions
	for (int i = 0; i < g_Points.size(); i++)
	{
		//collision with walls
		if (g_Points[i].position.x < playingField.x * -0.5f)
		{
			g_Points[i].position.x = playingField.x * -0.5f;
			g_Points[i].velocity.x = 0.0f;
			g_Points[i].force.x = 0.0f;

			col_Wall[2] = true;
		}

		if (g_Points[i].position.x > playingField.x * 0.5f)
		{
			g_Points[i].position.x = playingField.x * 0.5f;
			g_Points[i].velocity.x = 0.0f;
			g_Points[i].force.x = 0.0f;

			col_Wall[3] = true;
		}

		if (g_Points[i].position.z < playingField.z * - 0.5f)
		{
			g_Points[i].position.z = playingField.z * -0.5f;
			g_Points[i].velocity.z = 0.0f;
			g_Points[i].force.z = 0.0f;

			col_Wall[1] = true;
		}

		if (g_Points[i].position.z > playingField.z * 0.5f)
		{
			g_Points[i].position.z = playingField.z * 0.5f;
			g_Points[i].velocity.z = 0.0f;
			g_Points[i].force.z = 0.0f;

			col_Wall[0] = true;
		}
	}

	calculateCollisionForAll();
	calculateCollisionForAll_coupled();//calculate collisions for all Point/Box combinations


	//Rigidbodies update
	for (int i = 0; i < g_Boxes.size(); i++)
	{
		//add gravity
		if (g_Boxes[i].rigidBody.getInverseMass() > 0)
		{
			XMFLOAT3 gravity(0, -1.0f, 0);
			XMFLOAT3 gravForce = gravity * 1.0f / g_Boxes[i].rigidBody.getInverseMass();

			g_Boxes[i].rigidBody.applyForceAtPoint(gravForce, g_Boxes[i].rigidBody.getPosition());
		}
		g_Boxes[i].update(timeStep);
	}

	//Mass Spring System update
	midpointStep(g_Points, g_Springs, calculateSpringForcesGravityDamping, timeStep);


	//delet Boxes that have fallen out of sight
	removeOutOfBoundsBoxes();
}

void UpdateCoupledDemo2(float timeStep)
{
	calculateCollisionForAll();
	calculateCollisionForAll_coupled();//calculate collisions for all Point/Box combinations


	//Rigidbodies update
	for (int i = 0; i < g_Boxes.size(); i++)
	{
		//add gravity
		if (g_Boxes[i].rigidBody.getInverseMass() > 0)
		{
			XMFLOAT3 gravity(0, -1.0f, 0);
			XMFLOAT3 gravForce = gravity * 1.0f / g_Boxes[i].rigidBody.getInverseMass();

			g_Boxes[i].rigidBody.applyForceAtPoint(gravForce, g_Boxes[i].rigidBody.getPosition());
		}
		g_Boxes[i].update(timeStep);
	}

	//Mass Spring System update
	midpointStep(g_Points, g_Springs, calculateSpringForcesGravityDamping, timeStep);


	//delete Boxes that have fallen out of sight
	removeOutOfBoundsBoxes();
}
#endif


// ============================================================
// DXUT Callbacks
// ============================================================


//--------------------------------------------------------------------------------------
// Reject any D3D11 devices that aren't acceptable by returning false
//--------------------------------------------------------------------------------------
bool CALLBACK IsD3D11DeviceAcceptable( const CD3D11EnumAdapterInfo *AdapterInfo, UINT Output, const CD3D11EnumDeviceInfo *DeviceInfo,
                                       DXGI_FORMAT BackBufferFormat, bool bWindowed, void* pUserContext )
{
	return true;
}


//--------------------------------------------------------------------------------------
// Called right before creating a device, allowing the app to modify the device settings as needed
//--------------------------------------------------------------------------------------
bool CALLBACK ModifyDeviceSettings( DXUTDeviceSettings* pDeviceSettings, void* pUserContext )
{
	return true;
}


//--------------------------------------------------------------------------------------
// Create any D3D11 resources that aren't dependent on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11CreateDevice( ID3D11Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
	HRESULT hr;

    ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();;

    std::wcout << L"Device: " << DXUTGetDeviceStats() << std::endl;
    
    // Load custom effect from "effect.fxo" (compiled "effect.fx")
	std::wstring effectPath = GetExePath() + L"effect.fxo";
	if(FAILED(hr = D3DX11CreateEffectFromFile(effectPath.c_str(), 0, pd3dDevice, &g_pEffect)))
	{
        std::wcout << L"Failed creating effect with error code " << int(hr) << std::endl;
		return hr;
	}

    // Init AntTweakBar GUI
	TwInit(TW_DIRECT3D11, pd3dDevice);
    InitTweakBar(pd3dDevice);

    // Create DirectXTK geometric primitives for later usage
    g_pSphere = GeometricPrimitive::CreateGeoSphere(pd3dImmediateContext, 2.0f, 2, false);
    g_pTeapot = GeometricPrimitive::CreateTeapot(pd3dImmediateContext, 1.5f, 8, false);
	g_pCube = GeometricPrimitive::CreateCube(pd3dImmediateContext, 1.0f, false);
	

    // Create effect, input layout and primitive batch for position/color vertices (DirectXTK)
    {
        // Effect
        g_pEffectPositionColor = new BasicEffect(pd3dDevice);
        g_pEffectPositionColor->SetVertexColorEnabled(true); // triggers usage of position/color vertices

        // Input layout
        void const* shaderByteCode;
        size_t byteCodeLength;
        g_pEffectPositionColor->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);
        
        pd3dDevice->CreateInputLayout(VertexPositionColor::InputElements,
                                      VertexPositionColor::InputElementCount,
                                      shaderByteCode, byteCodeLength,
                                      &g_pInputLayoutPositionColor);

        // Primitive batch
        g_pPrimitiveBatchPositionColor = new PrimitiveBatch<VertexPositionColor>(pd3dImmediateContext);
    }

    // Create effect, input layout and primitive batch for position/normal vertices (DirectXTK)
    {
        // Effect
        g_pEffectPositionNormal = new BasicEffect(pd3dDevice);
        g_pEffectPositionNormal->EnableDefaultLighting(); // triggers usage of position/normal vertices
        g_pEffectPositionNormal->SetPerPixelLighting(true);

        // Input layout
        void const* shaderByteCode;
        size_t byteCodeLength;
        g_pEffectPositionNormal->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

        pd3dDevice->CreateInputLayout(VertexPositionNormal::InputElements,
                                      VertexPositionNormal::InputElementCount,
                                      shaderByteCode, byteCodeLength,
                                      &g_pInputLayoutPositionNormal);

        // Primitive batch
        g_pPrimitiveBatchPositionNormal = new PrimitiveBatch<VertexPositionNormal>(pd3dImmediateContext);
    }

    // Create effect, input layout and primitive batch for position/normal/color vertices (DirectXTK)
    {
        // Effect
        g_pEffectPositionNormalColor = new BasicEffect(pd3dDevice);
        g_pEffectPositionNormalColor->SetPerPixelLighting(true);
        g_pEffectPositionNormalColor->EnableDefaultLighting();     // triggers usage of position/normal/color vertices
        g_pEffectPositionNormalColor->SetVertexColorEnabled(true); // triggers usage of position/normal/color vertices

        // Input layout
        void const* shaderByteCode;
        size_t byteCodeLength;
        g_pEffectPositionNormalColor->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

        pd3dDevice->CreateInputLayout(VertexPositionNormalColor::InputElements,
                                      VertexPositionNormalColor::InputElementCount,
                                      shaderByteCode, byteCodeLength,
                                      &g_pInputLayoutPositionNormalColor);

        // Primitive batch
        g_pPrimitiveBatchPositionNormalColor = new PrimitiveBatch<VertexPositionNormalColor>(pd3dImmediateContext);
    }

	g_pPd3Device = pd3dDevice;

#ifdef RIGIDBODY

#endif

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11CreateDevice 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11DestroyDevice( void* pUserContext )
{
	SAFE_RELEASE(g_pEffect);
	
    TwDeleteBar(g_pTweakBar);
    g_pTweakBar = nullptr;
	TwTerminate();

    g_pSphere.reset();
    g_pTeapot.reset();
	g_pCube.reset();
    
    SAFE_DELETE (g_pPrimitiveBatchPositionColor);
    SAFE_RELEASE(g_pInputLayoutPositionColor);
    SAFE_DELETE (g_pEffectPositionColor);

    SAFE_DELETE (g_pPrimitiveBatchPositionNormal);
    SAFE_RELEASE(g_pInputLayoutPositionNormal);
    SAFE_DELETE (g_pEffectPositionNormal);

    SAFE_DELETE (g_pPrimitiveBatchPositionNormalColor);
    SAFE_RELEASE(g_pInputLayoutPositionNormalColor);
    SAFE_DELETE (g_pEffectPositionNormalColor);
}

//--------------------------------------------------------------------------------------
// Create any D3D11 resources that depend on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11ResizedSwapChain( ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
                                          const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
    // Update camera parameters
	int width = pBackBufferSurfaceDesc->Width;
	int height = pBackBufferSurfaceDesc->Height;
	g_camera.SetWindow(width, height);
	g_camera.SetProjParams(XM_PI / 4.0f, float(width) / float(height), 0.1f, 100.0f);

    // Inform AntTweakBar about back buffer resolution change
  	TwWindowSize(pBackBufferSurfaceDesc->Width, pBackBufferSurfaceDesc->Height);

#if defined(MASS_SPRING_SYSTEM) || defined(COUPLEDSIM)
	window_height = height;
	window_width = width;

#endif

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11ResizedSwapChain 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11ReleasingSwapChain( void* pUserContext )
{
}

//--------------------------------------------------------------------------------------
// Handle key presses
//--------------------------------------------------------------------------------------
void CALLBACK OnKeyboard( UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext )
{
    HRESULT hr;

#ifdef COUPLEDSIM
	keys[0] = false;
	keys[1] = false;
	keys[2] = false;
	keys[3] = false;
#endif

	if(bKeyDown)
	{
		switch(nChar)
		{
            // RETURN: toggle fullscreen
			case VK_RETURN :
			{
				if(bAltDown) DXUTToggleFullScreen();
				break;
			}
            // F8: Take screenshot
			case VK_F8:
			{
                // Save current render target as png
                static int nr = 0;
				std::wstringstream ss;
				ss << L"Screenshot" << std::setfill(L'0') << std::setw(4) << nr++ << L".png";

                ID3D11Resource* pTex2D = nullptr;
                DXUTGetD3D11RenderTargetView()->GetResource(&pTex2D);
                SaveWICTextureToFile(DXUTGetD3D11DeviceContext(), pTex2D, GUID_ContainerFormatPng, ss.str().c_str());
                SAFE_RELEASE(pTex2D);

                std::wcout << L"Screenshot written to " << ss.str() << std::endl;
				break;
			}
            // F10: Toggle video recording
            case VK_F10:
            {
                if (!g_pFFmpegVideoRecorder) {
                    g_pFFmpegVideoRecorder = new FFmpeg(25, 21, FFmpeg::MODE_INTERPOLATE);
                    V(g_pFFmpegVideoRecorder->StartRecording(DXUTGetD3D11Device(), DXUTGetD3D11RenderTargetView(), "output.avi"));
                } else {
                    g_pFFmpegVideoRecorder->StopRecording();
                    SAFE_DELETE(g_pFFmpegVideoRecorder);
                }
            }	
			case VK_UP:
			{
#ifdef RIGIDBODY
				if (g_iTestCase == 1 || g_iTestCase == 3)
				{
					//Demo 2
					for (int i = 0; i < g_Boxes.size(); i++)
					{
						g_Boxes[i].rigidBody.applyForceAtAllPoints(XMFLOAT3(0, 1, 0));
					}				
					break;
				}
#endif

#ifdef COUPLEDSIM
				if (g_iTestCase == 0)
				{
					keys[0] = true;
				}
				break;
#endif
			}
			case VK_DOWN:
			{
#ifdef RIGIDBODY
				if (g_iTestCase == 1 || g_iTestCase == 3)
				{
					//Demo 2
					for (int i = 0; i < g_Boxes.size(); i++)
					{
						g_Boxes[i].rigidBody.applyForceAtAllPoints(XMFLOAT3(0, -1, 0));
					}
					break;
				}
#endif

#ifdef COUPLEDSIM
				if (g_iTestCase == 0)
				{
					keys[1] = true;
				}
				break;
#endif
			}
			case VK_LEFT:
			{
#ifdef RIGIDBODY
				if (g_iTestCase == 1 || g_iTestCase == 3)
				{
					//Demo 2
					for (int i = 0; i < g_Boxes.size(); i++)
					{
						g_Boxes[i].rigidBody.applyForceAtAllPoints(XMFLOAT3(-1, 0, 0));
					}
					break;
				}
#endif
#ifdef COUPLEDSIM
				if (g_iTestCase == 0)
				{
					keys[2] = true;
				}
				break;
#endif
			}
			case VK_RIGHT:
			{
#ifdef RIGIDBODY
				if (g_iTestCase == 1 || g_iTestCase == 3)
				{
					//Demo 2
					for (int i = 0; i < g_Boxes.size(); i++)
					{
						g_Boxes[i].rigidBody.applyForceAtAllPoints(XMFLOAT3(1, 0, 0));
					}
					break;
				}
#endif

#ifdef COUPLEDSIM
				if (g_iTestCase == 0)
				{
					keys[3] = true;
				}
				break;
#endif
			}
		}
	}
}


//--------------------------------------------------------------------------------------
// Handle mouse button presses
//--------------------------------------------------------------------------------------
void CALLBACK OnMouse( bool bLeftButtonDown, bool bRightButtonDown, bool bMiddleButtonDown,
                       bool bSideButton1Down, bool bSideButton2Down, int nMouseWheelDelta,
                       int xPos, int yPos, void* pUserContext )
{

#if defined (MASS_SPRING_SYSTEM)
	switch (g_iTestCase)
	{
	case 1:
		// Track mouse movement if left mouse key is pressed
		{
			static int xPosSave = 0, yPosSave = 0;

			if (bLeftButtonDown)
			{
				// Accumulate deltas in g_viMouseDelta
				g_viMouseDelta.x += xPos - xPosSave;
				g_viMouseDelta.y += yPos - yPosSave;
			}

			xPosSave = xPos;
			yPosSave = yPos;
		}
		break;
	case 3:
		{
			  //Demo 4

			  if (bLeftButtonDown)
			  {

				  //mouse Coordinates in (-1, 1)
				  float xPos_normalized = 2.0f * (xPos / (float)window_width) - 1.0f;
				  float yPos_normalized = -2.0f * (yPos / (float)window_height) + 1.0f;


				  //transform to world space
				  XMMATRIX viewProj = g_camera.GetViewMatrix() * g_camera.GetProjMatrix();
				  XMMATRIX inverseViewProj = XMMatrixInverse(nullptr, viewProj);

				  XMFLOAT3 mousePosNear = XMFLOAT3(xPos_normalized, yPos_normalized, 1.0f - g_camera.GetNearClip());//point on the near plane
				  //XMFLOAT3 mousePosFar = XMFLOAT3(xPos_normalized, yPos_normalized, 100.0f);

				  XMVECTOR worldNear = XMVector3Transform(XMLoadFloat3(&mousePosNear), inverseViewProj);
				  //XMVECTOR worldFar = XMVector3Transform(XMLoadFloat3(&mousePosFar), inverseViewProj);



				

				  
				  //get camera position
				  XMFLOAT3 cameraPos;
				  XMStoreFloat3(&cameraPos, g_camera.GetEyePt());

				  XMFLOAT3 rayOrigin, rayEnd;
				  XMStoreFloat3(&rayEnd, worldNear);
				  rayOrigin = cameraPos;


				  XMFLOAT3 rayDirection = rayEnd + -1.0f * rayOrigin;
				  rayDirection = rayDirection * (1.0f / magnitude(rayDirection));

				  rayEnd = rayOrigin + 10.0f * rayDirection;



				  //rayCast to find the MassPoint under mouse
				  XMFLOAT3 nearestMousePos;
				  int hitIndex = rayCast(rayOrigin, rayDirection, nearestMousePos);

				  if (indexBeingDragged == -1 && hitIndex != -1)//if we hit something
				  {
					  indexBeingDragged = hitIndex;
				  }

				  if (indexBeingDragged != -1)
				  {
					  g_Points[indexBeingDragged].isFixed = true;
					  g_Points[indexBeingDragged].force = XMFLOAT3(0, 0, 0);
					  g_Points[indexBeingDragged].velocity = XMFLOAT3(0, 0, 0);
					  g_Points[indexBeingDragged].position = nearestMousePos;
				  }
			  }
			  else
			  {
				  indexBeingDragged = -1;//stop drag & drop

				  //un-pin all points
				  for (int i = 0; i < g_Points.size(); i++)
				  {
					  g_Points[i].isFixed = false;
				  }
			  }
		}
		break;
	default:
		break;
	}
#endif

#if defined(COUPLEDSIM)

#endif
   
}


//--------------------------------------------------------------------------------------
// Handle messages to the application
//--------------------------------------------------------------------------------------
LRESULT CALLBACK MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam,
                          bool* pbNoFurtherProcessing, void* pUserContext )
{
    // Send message to AntTweakbar first
    if (TwEventWin(hWnd, uMsg, wParam, lParam))
    {
        *pbNoFurtherProcessing = true;
        return 0;
    }

    // If message not processed yet, send to camera
	if(g_camera.HandleMessages(hWnd,uMsg,wParam,lParam))
    {
        *pbNoFurtherProcessing = true;
		return 0;
    }

	return 0;
}


//--------------------------------------------------------------------------------------
// Handle updates to the scene
//--------------------------------------------------------------------------------------
void CALLBACK OnFrameMove(double dTime, float fElapsedTime, void* pUserContext)
{
	UpdateWindowTitle(L"Demo");

	// Move camera
	g_camera.FrameMove(fElapsedTime);

	// Update effects with new view + proj transformations
	g_pEffectPositionColor->SetView(g_camera.GetViewMatrix());
	g_pEffectPositionColor->SetProjection(g_camera.GetProjMatrix());

	g_pEffectPositionNormal->SetView(g_camera.GetViewMatrix());
	g_pEffectPositionNormal->SetProjection(g_camera.GetProjMatrix());

	g_pEffectPositionNormalColor->SetView(g_camera.GetViewMatrix());
	g_pEffectPositionNormalColor->SetProjection(g_camera.GetProjMatrix());

#ifdef TEMPLATE_DEMO

	if (g_iPreTestCase != g_iTestCase){// test case changed
		// clear old setup and build up new setup
		TwDeleteBar(g_pTweakBar);
		g_pTweakBar = nullptr;
		InitTweakBar(g_pPd3Device);
		switch (g_iTestCase)
		{
		case 0:
			cout << "Basic Test!\n";
			g_bDrawSpheres = true;
			g_iNumSpheres = 100;
			g_fSphereSize = 0.05f;
			break;
		case 1:
			cout << "Test1!\n";
			g_bDrawTeapot = true;			
			g_vfMovableObjectPos = XMFLOAT3(0, 0, 0);
			g_vfRotate = XMFLOAT3(0, 0, 0);
			break;
		case 2:
			cout << "Test2!\n";
			g_bDrawTriangle = true;
			break;
		default:
			cout << "Empty Test!\n";
			break;
		}
		g_iPreTestCase = g_iTestCase;
	}
	if (g_bSimulateByStep && DXUTIsKeyDown(VK_SPACE)){
		g_bIsSpaceReleased = false;
	}
	if (g_bSimulateByStep && !g_bIsSpaceReleased)
		if (DXUTIsKeyDown(VK_SPACE))
			return;
	if (g_bSimulateByStep && g_bIsSpaceReleased)
		return;
	// update current setup for each frame
	switch (g_iTestCase)
	{// handling different cases
	case 1:
		// Apply accumulated mouse deltas to g_vfMovableObjectPos (move along cameras view plane)
		if (g_viMouseDelta.x != 0 || g_viMouseDelta.y != 0)
		{
			// Calcuate camera directions in world space
			XMMATRIX viewInv = XMMatrixInverse(nullptr, g_camera.GetViewMatrix());
			XMVECTOR camRightWorld = XMVector3TransformNormal(g_XMIdentityR0, viewInv);
			XMVECTOR camUpWorld = XMVector3TransformNormal(g_XMIdentityR1, viewInv);

			// Add accumulated mouse deltas to movable object pos
			XMVECTOR vMovableObjectPos = XMLoadFloat3(&g_vfMovableObjectPos);

			float speedScale = 0.001f;
			vMovableObjectPos = XMVectorAdd(vMovableObjectPos, speedScale * (float)g_viMouseDelta.x * camRightWorld);
			vMovableObjectPos = XMVectorAdd(vMovableObjectPos, -speedScale * (float)g_viMouseDelta.y * camUpWorld);

			XMStoreFloat3(&g_vfMovableObjectPos, vMovableObjectPos);

			// Reset accumulated mouse deltas
			g_viMouseDelta = XMINT2(0, 0);
		}
		// rotate the teapot
		g_vfRotate.x += 0.005f;
		if (g_vfRotate.x > 2 * M_PI) g_vfRotate.x -= 2 * M_PI;
		g_vfRotate.y += 0.005f;
		if (g_vfRotate.y > 2 * M_PI) g_vfRotate.y -= 2 * M_PI;
		g_vfRotate.z += 0.005f;
		if (g_vfRotate.z > 2 * M_PI) g_vfRotate.z -= 2 * M_PI;

		break;
	default:
		break;
	}
	if (g_bSimulateByStep)
		g_bIsSpaceReleased = true;

#endif

#ifdef MASS_SPRING_SYSTEM

	// test case changed
	if (g_iPreTestCase != g_iTestCase)
	{

		// clear old setup and build up new setup
		TwDeleteBar(g_pTweakBar);
		g_pTweakBar = nullptr;
		InitTweakBar(g_pPd3Device);

		//decide what to do depending on demo case
		switch (g_iTestCase)
		{
		case 0:
		{
			//Demo 1
			std::vector<MassPoint> pointsEuler, pointsMidpoint;
			std::vector<Spring> springs;

			//Set up points
			MassPoint point1(XMFLOAT3(0, 0, 0), XMFLOAT3(-1, 0, 0), 10.0f);
			MassPoint point2(XMFLOAT3(0, 2, 0), XMFLOAT3(1, 0, 0), 10.0f);

			pointsEuler.push_back(point1);
			pointsEuler.push_back(point2);

			pointsMidpoint.push_back(point1);
			pointsMidpoint.push_back(point2);

			//Set up Spring
			Spring connection(0, 1, 1.0f, 40.0f);
			springs.push_back(connection);


			//calculate euler step
			eulerStep(pointsEuler, springs, calculateSpringForces, 0.1f);

			cout << "Euler Integration: \n\n";

			cout << "p0 (t + h): " << pointsEuler[0].position << "\n";
			cout << "p1 (t + h): " << pointsEuler[1].position << "\n";
			cout << "v0 (t + h): " << pointsEuler[0].velocity << "\n";
			cout << "v1 (t + h): " << pointsEuler[1].velocity << "\n\n";

			//calculate midpoint step
			midpointStep(pointsMidpoint, springs, calculateSpringForces, 0.1f);

			cout << "Midpoint Integration: \n\n";

			cout << "p0 (t + h): " << pointsMidpoint[0].position << "\n";
			cout << "p1 (t + h): " << pointsMidpoint[1].position << "\n";
			cout << "v0 (t + h): " << pointsMidpoint[0].velocity << "\n";
			cout << "v1 (t + h): " << pointsMidpoint[1].velocity << "\n\n";



			DXUTShutdown();//end main loop
			system("pause");//keep console open
			break;
		}

		case 1:
		{
			//Demo 2
			g_Points.clear();
			g_Springs.clear();

			MassPoint point1(XMFLOAT3(0, 0, 0), XMFLOAT3(-1, 0, 0), 10.0f);
			MassPoint point2(XMFLOAT3(0, 2, 0), XMFLOAT3(1, 0, 0), 10.0f);

			g_Points.push_back(point1);
			g_Points.push_back(point2);

			Spring connection(0, 1, 1.0f, 40.0f);
			g_Springs.push_back(connection);

			break;
		}
		case 2:
		{
			//Demo 3
			g_Points.clear();
			g_Springs.clear();

			MassPoint point1(XMFLOAT3(0, 0, 0), XMFLOAT3(-1, 0, 0), 10.0f);
			MassPoint point2(XMFLOAT3(0, 2, 0), XMFLOAT3(1, 0, 0), 10.0f);

			g_Points.push_back(point1);
			g_Points.push_back(point2);

			Spring connection(0, 1, 1.0f, 40.0f);
			g_Springs.push_back(connection);

			break;
		}
		case 3:
		{
			//Demo 4
			g_Points.clear();
			g_Springs.clear();

			createCube(XMFLOAT3(-0.35f, 0, 0), 0.25f, 1000.0f, 80.0f);
			createCube(XMFLOAT3(0.1f, 0, 0), 0.25f, 400.0f, 80.0f);

			/*
			g_Points.clear();
			g_Springs.clear();

			MassPoint point1(XMFLOAT3(-0.5f, 0.5f, 0), XMFLOAT3(0, 0, 0), 10.0f);
			MassPoint point2(XMFLOAT3(0, 0.5f, 0), XMFLOAT3(0, 0, 0), 10.0f);
			point2.color = XMFLOAT3(0.0f, 1.0f, 0.0f);
			MassPoint point3(XMFLOAT3(0.5f, 0.5f, 0), XMFLOAT3(0, 0, 0), 10.0f);
			point3.color = XMFLOAT3(0.0f, 0.0f, 1.0f);

			g_Points.push_back(point1);
			g_Points.push_back(point2);
			g_Points.push_back(point3);

			Spring connection1(0, 1, 1.0f, 40.0f);
			Spring connection2(1, 2, 1.0f, 40.0f);
			g_Springs.push_back(connection1);
			g_Springs.push_back(connection2);*/

			break;
		}
		default:
			cout << "Empty Test!\n";
			break;
		}

		g_iPreTestCase = g_iTestCase;
	}

	switch (g_iTestCase)
	{
	case 1:
		UpdateDemo2(fElapsedTime);
		break;
	case 2:
		UpdateDemo3(fElapsedTime);
		break;
	case 3:
		UpdateDemo4(fElapsedTime);
		break;
	default:
		break;
	}
#endif

#ifdef RIGIDBODY
	
	
	/*
	box1.rigidBody.applyForceAtAllPoints(XMFLOAT3(0, -0.1f, 0));

	box1.update(fElapsedTime);
	box2.update(fElapsedTime);
	*/

	/*CollisionInfo colResult = checkCollision(box1.getWorldMatrix(), box2.getWorldMatrix());

	//if a collision occurred
	if (colResult.isValid)
	{
		//NOT REALLY TESTED
		XMFLOAT3 normal, colPoint;
		XMStoreFloat3(&normal, colResult.normalWorld);
		XMStoreFloat3(&colPoint, colResult.collisionPointWorld);

		//collision response for (B, A)
		float impulse = RigidBody::calculateImpulse(box1.rigidBody, box2.rigidBody, -1.0f * normal, colPoint, 0.0f);


		box1.rigidBody.applyImpulseAtPoint(colPoint, normal, impulse);
		box2.rigidBody.applyImpulseAtPoint(colPoint, normal, -impulse);

	}
	else
	{
		//check pair of boxes the other way around
		colResult = checkCollision(box2.getWorldMatrix(), box1.getWorldMatrix());

		if (colResult.isValid)
		{
			
			XMFLOAT3 normal, colPoint;
			XMStoreFloat3(&normal, colResult.normalWorld);
			XMStoreFloat3(&colPoint, colResult.collisionPointWorld);

			//collision response for (B, A)
			float impulse = RigidBody::calculateImpulse(box2.rigidBody, box1.rigidBody, -1.0f * normal, colPoint, 0.0f);

			
			box2.rigidBody.applyImpulseAtPoint(colPoint, normal, impulse);
			box1.rigidBody.applyImpulseAtPoint(colPoint, normal, -impulse);


		}
	} */


	//DEMOS
	// test case changed
	if (g_iPreTestCase != g_iTestCase)
	{

		// clear old setup and build up new setup
		TwDeleteBar(g_pTweakBar);
		g_pTweakBar = nullptr;
		InitTweakBar(g_pPd3Device);

		//decide what to do depending on demo case
		switch (g_iTestCase)
		{
		case 0:
		{
				  //Demo 1

				  RigidBox box1(XMFLOAT3(0, 0, 0), XMFLOAT3(1.0f, 0.6f, 0.5f), 2.0f, XMFLOAT3(0, 0, M_PI / 2.0f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));
				  
				  int observedPointIndex = box1.rigidBody.getMassPoint(XMFLOAT3(-0.3f, -0.5f, -0.25f));

				  box1.rigidBody.applyForceAtPoint(XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT3(0.3f, 0.5f, 0.25f));				  


				  g_Boxes.clear();
				  g_Boxes.push_back(box1);

				  box1.update(2.0f);

				  cout << "Angular Velocity: " << box1.rigidBody.getAngularVelocity() << "\n";
				  cout << "Linear Velocity: " << box1.rigidBody.getLinearVelocity() << "\n";
				  cout << "World Space Velocity: " << box1.rigidBody.getMassPoint(observedPointIndex).velocity_world << "\n";

				  DXUTShutdown();//end main loop
				  system("pause");//keep console open
				  break;
		}

		case 1:
		{
				  //Demo 2

				  RigidBox box1(XMFLOAT3(0, 0, 0), XMFLOAT3(1.0f, 0.6f, 0.5f), 2.0f, XMFLOAT3(0, 0, M_PI / 2.0f), XMFLOAT3(0, 0, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));

				  int observedPointIndex = box1.rigidBody.getMassPoint(XMFLOAT3(-0.3f, -0.5f, -0.25f));

				  box1.rigidBody.applyForceAtPoint(XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT3(0.3f, 0.5f, 0.25f));


				  g_Boxes.clear();
				  g_Boxes.push_back(box1);

				  break;				  
		}
		case 2:
		{
				  //Demo 3
				  RigidBox box1(XMFLOAT3(0.25f, 0.25f, 0), XMFLOAT3(0.15f, 0.15f, 0.15f), 2.0f, XMFLOAT3(0, 0, 0), XMFLOAT3(0, -1, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));
				  RigidBox box2(XMFLOAT3(0, -0.5f, 0), XMFLOAT3(0.75f, 1.00f, 0.75f), 200.0f, XMFLOAT3(0, 0, 0), XMFLOAT3(0, 0, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));

				  g_Boxes.clear();
				  g_Boxes.push_back(box1);
				  g_Boxes.push_back(box2);

				  break;
		}
		case 3:
		{
				  //Demo 4
				  RigidBox box0(XMFLOAT3(0, -1.0f, 0), XMFLOAT3(3.0f, 0.1f, 3.0f), 20.0f, XMFLOAT3(0, 0, 0), XMFLOAT3(0, 0, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));
				  RigidBox box1(XMFLOAT3(0.25f, 0.25f, 0), XMFLOAT3(0.15f, 0.15f, 0.15f), 20.0f, XMFLOAT3(0, 0, 0), XMFLOAT3(0, 0, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));
				  RigidBox box2(XMFLOAT3(0, -0.2f, 0), XMFLOAT3(0.5f, 0.3f, 0.5f), 200.0f, XMFLOAT3(M_PI / 2.5f, M_PI / 2.5f, 0), XMFLOAT3(0, 0, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));
				  RigidBox box3(XMFLOAT3(0, 0.8f, 0), XMFLOAT3(0.15f, 0.15f, 0.8f), 80.0f, XMFLOAT3(M_PI / 2.5f, 0, M_PI / 2.5f), XMFLOAT3(0, 0.0f, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));
				  RigidBox box4(XMFLOAT3(0.25f, 1.0f, 0), XMFLOAT3(0.15f, 0.15f, 0.15f), 20.0f, XMFLOAT3(0, 0, 0), XMFLOAT3(0, 0, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));

				  box0.rigidBody.immobilize();

				  g_Boxes.clear();
				  g_Boxes.push_back(box0);
				  g_Boxes.push_back(box1);
				  g_Boxes.push_back(box2);
				  g_Boxes.push_back(box3);
				  g_Boxes.push_back(box4);

				  break;
		}
		default:
			cout << "Empty Test!\n";
			break;
		}

		g_iPreTestCase = g_iTestCase;

	}

	switch (g_iTestCase)
	{
	case 1:
		UpdateRigidDemo(0.01f);
		break;
	case 2:
		UpdateRigidDemo(0.01f);
		break;
	case 3:
		UpdateRigidDemo4(0.01f);
		break;
	default:
		break;
	}
#endif

#ifdef FLUIDSIM
	if (g_iPreTestCase != g_iTestCase)
	{
		switch (g_iTestCase)
		{
		case 0:
		{
			//formerly blobX, now blobz
			createBlobCube(6);
			break;
		}
		case 1:
		{
			createBlobCube(6);
			g_BlobsPointer.clear();
			for (int i = 0; i < g_Blobs.size(); i++)
			{
				g_BlobsPointer.push_back(&g_Blobs[i]);
			}
			break;
		}
			
		case 2:
			break;
		case 3:
			break;
		default:
			break;
		}
		g_iPreTestCase = g_iTestCase;
	}

	switch (g_iTestCase)
	{
	case 0:
		UpdateFluidDemo1(0.001f);
		break;
	case 1:
		UpdateFluidDemo2(0.001f);
		break;
	case 2:
		UpdateFluidDemo3(0.001f);
		break;
	case 3:
		UpdateFluidDemo4(0.001f);
		break;
	default:
		break;
	}
	
#endif

#ifdef COUPLEDSIM

	// test case changed
	if (g_iPreTestCase != g_iTestCase)
	{

		// clear old setup and build up new setup
		TwDeleteBar(g_pTweakBar);
		g_pTweakBar = nullptr;
		InitTweakBar(g_pPd3Device);

		//decide what to do depending on demo case
		switch (g_iTestCase)
		{
		case 0:
		{
			//SCENE SETUP GAME
			srand(unsigned(time));

			//MASS_SPRING_SYSTEMS
			g_Points.clear();
			g_Springs.clear();

			//createCube(XMFLOAT3(0, 0, 0), 0.25f, 1000.0f, 80.0f);
			//createCube(XMFLOAT3(0.1f, 0, 0), 0.25f, 400.0f, 80.0f);
			//g_Points.push_back(MassPoint(XMFLOAT3(0, 0.24f, 0), XMFLOAT3(0, 0, 0), 1.0f));
			float massPerPoint = 5.0f;
			int res = 4;

			createGrid(XMFLOAT3(0, -0.1f, 0), XMFLOAT3(0.5f, 0.4f, 0.5f), XMFLOAT3(0.3f, 0.3f, 0.3f), 5000.0f, massPerPoint * pow(res, 3), res);


			//RIGIDBODIES
			g_Boxes.clear();
			RigidBox box1(XMFLOAT3(0, -0.5f, 0), XMFLOAT3(playingField.x + 0.1f, 0.1f, playingField.z + 0.1f), 2000.0f, XMFLOAT3(0, 0, 0), XMFLOAT3(0, 0, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));			
			box1.rigidBody.immobilize();
			box1.color = XMFLOAT3(0.75f, 0.75f, 0.75f);

			g_Boxes.push_back(box1);

			break;
		}

		case 1:
		{
			//Demo 2

			//MASS_SPRING_SYSTEMS
			g_Points.clear();
			g_Springs.clear();

			//createCube(XMFLOAT3(0, 0, 0), 0.25f, 1000.0f, 80.0f);
			//createCube(XMFLOAT3(0.1f, 0, 0), 0.25f, 400.0f, 80.0f);
			//g_Points.push_back(MassPoint(XMFLOAT3(0, 0.24f, 0), XMFLOAT3(0, 0, 0), 1.0f));
			float massPerPoint = 5.0f;
			int res = 4;

			createGrid(XMFLOAT3(0, -0.1f, 0), XMFLOAT3(0.5f, 0.4f, 0.5f), XMFLOAT3(0.75f, 0.75f, 0.75f), 5000.0f, massPerPoint * pow(res, 3), res);


			//RIGIDBODIES
			g_Boxes.clear();
			RigidBox box1(XMFLOAT3(0, -0.5f, 0), XMFLOAT3(3.0f, 0.1f, 3.0f), 2000.0f, XMFLOAT3(0, 0, 0), XMFLOAT3(0, 0, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));
			RigidBox box2(XMFLOAT3(0.0f, 1.5f, 0.0f), XMFLOAT3(0.25f, 0.25f, 0.25f), 100.0f, XMFLOAT3(0, 0, 0), XMFLOAT3(0, -2.5f, 0), XMFLOAT3(0.0f, 0.0f, 0.0f));
			box1.rigidBody.immobilize();

			g_Boxes.push_back(box1);
			g_Boxes.push_back(box2);
			break;
		}
		default:
			break;
		}

		g_iPreTestCase = g_iTestCase;
	}

	switch (g_iTestCase)
	{
	case 0:
		UpdateCoupledDemo1(time_step);
		break;
	case 1:
		UpdateCoupledDemo2(time_step);
		break;
	default:
		break;
	}
#endif
}

//--------------------------------------------------------------------------------------
// Render the scene using the D3D11 device
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11FrameRender( ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext,
                                  double fTime, float fElapsedTime, void* pUserContext )
{
    HRESULT hr;

	// Clear render target and depth stencil
	float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

	ID3D11RenderTargetView* pRTV = DXUTGetD3D11RenderTargetView();
	ID3D11DepthStencilView* pDSV = DXUTGetD3D11DepthStencilView();
	pd3dImmediateContext->ClearRenderTargetView( pRTV, ClearColor );
	pd3dImmediateContext->ClearDepthStencilView( pDSV, D3D11_CLEAR_DEPTH, 1.0f, 0 );

#ifndef COUPLEDSIM
    // Draw floor
    DrawFloor(pd3dImmediateContext);

    // Draw axis box
    DrawBoundingBox(pd3dImmediateContext);
#endif

#ifdef TEMPLATE_DEMO
	switch (g_iTestCase)
	{
	case 0:
		// Draw speheres
		if (g_bDrawSpheres) DrawSomeRandomObjects(pd3dImmediateContext);
		break;
	case 1:
		// Draw movable teapot
		if (g_bDrawTeapot) DrawMovableTeapot(pd3dImmediateContext);
		break;
	case 2:
		// Draw simple triangle
		if (g_bDrawTriangle) DrawTriangle(pd3dImmediateContext);
		break;
	default:
		break;
	}
#endif
    
#if defined(MASS_SPRING_SYSTEM) || defined(COUPLEDSIM)
	DrawMassSpringSystem(pd3dImmediateContext);
#endif

#if defined(RIGIDBODY) || defined(COUPLEDSIM)
	for (int i = 0; i < g_Boxes.size(); i++)
	{
		g_Boxes[i].configureDraw(g_pEffectPositionNormal);
		g_pCube->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
	}
#endif

#ifdef FLUIDSIM
	DrawFluid(pd3dImmediateContext);
#endif

    // Draw GUI
    TwDraw();

    if (g_pFFmpegVideoRecorder) 
    {
        V(g_pFFmpegVideoRecorder->AddFrame(pd3dImmediateContext, DXUTGetD3D11RenderTargetView()));
    }
}

//--------------------------------------------------------------------------------------
// Initialize everything and go into a render loop
//--------------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
#if defined(DEBUG) | defined(_DEBUG)
	// Enable run-time memory check for debug builds.
	// (on program exit, memory leaks are printed to Visual Studio's Output console)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

#ifdef _DEBUG
	std::wcout << L"---- DEBUG BUILD ----\n\n";
#endif

	// Set general DXUT callbacks
	DXUTSetCallbackMsgProc( MsgProc );
	DXUTSetCallbackMouse( OnMouse, true );
	DXUTSetCallbackKeyboard( OnKeyboard );

	DXUTSetCallbackFrameMove( OnFrameMove );
	DXUTSetCallbackDeviceChanging( ModifyDeviceSettings );

	// Set the D3D11 DXUT callbacks
	DXUTSetCallbackD3D11DeviceAcceptable( IsD3D11DeviceAcceptable );
	DXUTSetCallbackD3D11DeviceCreated( OnD3D11CreateDevice );
	DXUTSetCallbackD3D11SwapChainResized( OnD3D11ResizedSwapChain );
	DXUTSetCallbackD3D11FrameRender( OnD3D11FrameRender );
	DXUTSetCallbackD3D11SwapChainReleasing( OnD3D11ReleasingSwapChain );
	DXUTSetCallbackD3D11DeviceDestroyed( OnD3D11DestroyDevice );

    // Init camera
#ifndef COUPLEDSIM
    g_camera.SetButtonMasks(MOUSE_MIDDLE_BUTTON, MOUSE_WHEEL, MOUSE_RIGHT_BUTTON);

	XMFLOAT3 eye(0.0f, 0.0f, -2.0f);
	XMFLOAT3 lookAt(0.0f, 0.0f, 0.0f);
	g_camera.SetViewParams(XMLoadFloat3(&eye), XMLoadFloat3(&lookAt));
#else
	g_camera.SetButtonMasks(0, MOUSE_WHEEL, MOUSE_RIGHT_BUTTON);

	XMFLOAT3 eye(0.0f, 3.5f, -2.5f);
	XMFLOAT3 lookAt(0.0f, 0.0f, 0.0f);
	g_camera.SetViewParams(XMLoadFloat3(&eye), XMLoadFloat3(&lookAt));
#endif

    // Init DXUT and create device
	DXUTInit( true, true, NULL ); // Parse the command line, show msgboxes on error, no extra command line params
	//DXUTSetIsInGammaCorrectMode( false ); // true by default (SRGB backbuffer), disable to force a RGB backbuffer
	DXUTSetCursorSettings( true, true ); // Show the cursor and clip it when in full screen
	DXUTCreateWindow( L"Demo" );
	DXUTCreateDevice( D3D_FEATURE_LEVEL_11_0, true, 1280, 960 );
    
	DXUTMainLoop(); // Enter into the DXUT render loop

	DXUTShutdown(); // Shuts down DXUT (includes calls to OnD3D11ReleasingSwapChain() and OnD3D11DestroyDevice())
	
	return DXUTGetExitCode();
}
