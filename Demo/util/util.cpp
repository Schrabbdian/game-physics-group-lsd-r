#include "util.h"

#include <Windows.h>

#include <DXUT.h>


std::wstring GetExePath()
{
	// get full path to .exe
	const size_t bufferSize = 1024;
	wchar_t buffer[bufferSize];
	if(0 == GetModuleFileNameW(nullptr, buffer, bufferSize))
	{
		return std::wstring(L"");
	}
	std::wstring path(buffer);
	// extract path (remove filename)
	size_t posSlash = path.find_last_of(L"/\\");
	if(posSlash != std::wstring::npos)
	{
		path = path.substr(0, posSlash + 1);
	}
	return path;
}


void UpdateWindowTitle(const std::wstring& appName)
{
	// check if we should update the window title
	bool update = false;

	// update if window size changed
	static int s_windowWidth = 0;
	static int s_windowHeight = 0;
	if (s_windowWidth != DXUTGetWindowWidth() || s_windowHeight != DXUTGetWindowHeight()) {
		s_windowWidth = DXUTGetWindowWidth();
		s_windowHeight = DXUTGetWindowHeight();
		update = true;
	}

	// update if fps changed (updated once per second by DXUT)
	static float s_fps = 0.0f;
	static float s_mspf = 0.0f;
	if (s_fps != DXUTGetFPS()) {
		s_fps = DXUTGetFPS();
		s_mspf = 1000.0f / s_fps;
		update = true;
	}

	// update window title if something relevant changed
	if (update) {
		const size_t len = 512;
		wchar_t str[len];
		swprintf_s(str, len, L"%s %ux%u @ %.2f fps / %.2f ms", appName.c_str(), s_windowWidth, s_windowHeight, s_fps, s_mspf);
		SetWindowText(DXUTGetHWND(), str);
	}
}



//Operator Overloads
XMFLOAT3 operator+ (XMFLOAT3 left, XMFLOAT3 right)
{
	return XMFLOAT3(left.x + right.x, left.y + right.y, left.z + right.z);
}

XMFLOAT3 operator- (XMFLOAT3 left, XMFLOAT3 right)
{
	return XMFLOAT3(left.x - right.x, left.y - right.y, left.z - right.z);
}

XMFLOAT3 operator* (float scalar, XMFLOAT3 vector)
{
	return XMFLOAT3(scalar * vector.x, scalar * vector.y, scalar * vector.z);
}

XMFLOAT3 operator* (XMFLOAT3 vector, float scalar)
{
	return XMFLOAT3(scalar * vector.x, scalar * vector.y, scalar * vector.z);
}

XMFLOAT3 operator/ (XMFLOAT3 vector, float scalar)
{
	return XMFLOAT3(vector.x / scalar, vector.y / scalar, vector.z / scalar);
}

std::ostream& operator<< (std::ostream& left, XMFLOAT3 right)
{
	return left << "(" << right.x << ", " << right.y << ", " << right.z << ")";
}

bool operator== (XMFLOAT3 left, XMFLOAT3 right)
{
	return left.x == right.x && left.y == right.y && left.z == right.z;
}

bool operator!= (XMFLOAT3 left, XMFLOAT3 right)
{
	return !(left == right);
}

//helper methods
float magnitude(XMFLOAT3 vector)
{
	return sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
}

float dot(XMFLOAT3 a, XMFLOAT3 b)
{
	float result;
	result = a.x * b.x + a.y * b.y + a.z * b.z;

	return result;
}

XMFLOAT3 cross(XMFLOAT3 a, XMFLOAT3 b)
{
	XMFLOAT3 result;

	result.x = a.y * b.z - a.z * b.y;
	result.y = a.z * b.x - a.x * b.z;
	result.z = a.x * b.y - a.y * b.x;

	return result;
}

bool vectorsEqual(XMFLOAT3 a, XMFLOAT3 b)
{
	return (a.x == b.x && a.y == b.y && a.z == b.z);
}



// source file:
/* params:
obj2World_A, the transfer matrix from object space of A to the world space
obj2World_B, the transfer matrix from object space of B to the world space

--> checks whether any corners of B are inside of A
*/
CollisionInfo checkCollision(const XMMATRIX obj2World_A, const XMMATRIX obj2World_B) {

	// the transfer matrix from the world space to object space of A:
	const XMMATRIX world2Obj_A = XMMatrixInverse(nullptr, obj2World_A);
	// the transfer matrix from the object space of B to the object space of A:
	const XMMATRIX objB2objA = obj2World_B * world2Obj_A;

	// store the position of B's centre in the object space of A
	const XMVECTOR centerB_objA = XMVector3Transform(XMVectorZero(), objB2objA);
	// store all the edges of B in obj space of A
	XMVECTOR edgeB_objA[3];
	for (size_t i = 0; i < 3; ++i)// half edges are 0.5f
		edgeB_objA[i] = XMVector3TransformNormal(XMVectorSetByIndex(XMVectorZero(), 0.5f, i), objB2objA);
	// store corners of body B in object space A
	const XMVECTOR cornersB_objA[8] = {
		centerB_objA - edgeB_objA[0] - edgeB_objA[1] - edgeB_objA[2],
		centerB_objA + edgeB_objA[0] - edgeB_objA[1] - edgeB_objA[2],
		centerB_objA - edgeB_objA[0] + edgeB_objA[1] - edgeB_objA[2],
		centerB_objA + edgeB_objA[0] + edgeB_objA[1] - edgeB_objA[2],
		centerB_objA - edgeB_objA[0] - edgeB_objA[1] + edgeB_objA[2],
		centerB_objA + edgeB_objA[0] - edgeB_objA[1] + edgeB_objA[2],
		centerB_objA - edgeB_objA[0] + edgeB_objA[1] + edgeB_objA[2],
		centerB_objA + edgeB_objA[0] + edgeB_objA[1] + edgeB_objA[2]
	};
	// return data structure, initialized as no collision
	CollisionInfo info;
	info.isValid = false;
	info.collisionPointWorld = XMVectorZero();
	info.normalWorld = XMVectorZero();

	// check every corners of body B
	for (size_t i = 0; i < 8; ++i){
		if (XMVector3InBounds(cornersB_objA[i], XMVectorSet(0.5f, 0.5f, 0.5f, 0.0f))){
			//collision! find the closest face in A
			int   normalIndex = 0;
			float mindis = FLT_MAX;
			float disSign = 1.0;

			for (int j = 0; j < 3; ++j){
				float curpos = XMVectorGetByIndex(cornersB_objA[i], j);
				float curdis = 0.5f - abs(curpos);

				if (curdis < mindis) {
					normalIndex = j;
					mindis = curdis;
					disSign = (curpos >= 0.0f) ? -1.0f : 1.0f;
				}
			}
			// assert(normalIndex != -1);
			info.isValid = true;
			// transform collision points from object space into world space
			info.collisionPointWorld = XMVector3Transform(cornersB_objA[i], obj2World_A);
			// the direction of the collision is opposite to the normal of the collision 
			// face on A (the one nearest to the corner(B))
			info.normalWorld = XMVector3Normalize(XMVector3TransformNormal
				(XMVectorSetByIndex(XMVectorZero(), disSign, normalIndex), obj2World_A));

			return info;
		}
	}
	// no collision
	return info;
}

CollisionInfo checkCollision(const XMMATRIX obj2World_A, const XMVECTOR worldPoint) 

{
	// the transfer matrix from the world space to object space of A:
	const XMMATRIX world2Obj_A = XMMatrixInverse(nullptr, obj2World_A);
	
	// Transform worldPoint into A's object space
	XMVECTOR pointInObj_A = XMVector3Transform(worldPoint, world2Obj_A);


	// return data structure, initialized as no collision
	CollisionInfo info;
	info.isValid = false;
	info.collisionPointWorld = XMVectorZero();
	info.normalWorld = XMVectorZero();

	// check the worldPoint against object A
	if (XMVector3InBounds(pointInObj_A, XMVectorSet(0.5f, 0.5f, 0.5f, 0.0f))){
		//collision! find the closest face in A
		int   normalIndex = 0;
		float mindis = FLT_MAX;
		float disSign = 1.0;

		for (int j = 0; j < 3; ++j){
			float curpos = XMVectorGetByIndex(pointInObj_A, j);
			float curdis = 0.5f - abs(curpos);

			if (curdis < mindis) {
				normalIndex = j;
				mindis = curdis;
				disSign = (curpos >= 0.0f) ? -1.0f : 1.0f;
			}
		}
		// assert(normalIndex != -1);
		info.isValid = true;
		// transform collision points from object space into world space
		info.collisionPointWorld = XMVector3Transform(pointInObj_A, obj2World_A);
		// the direction of the collision is opposite to the normal of the collision 
		// face on A (the one nearest to the corner(B))
		info.normalWorld = XMVector3Normalize(XMVector3TransformNormal
			(XMVectorSetByIndex(XMVectorZero(), disSign, normalIndex), obj2World_A));

		XMVECTOR dist = XMVector3TransformNormal(info.normalWorld * mindis, obj2World_A);
		info.colDist = XMVectorGetX(XMVector3Length(dist));

		return info;
	}
	
	// no collision
	return info;
}
