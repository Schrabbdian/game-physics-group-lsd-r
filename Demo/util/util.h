#pragma once

#ifndef __util_h__
#define __util_h__


#include <string>

#include <DirectXMath.h>
#include <iostream>

using namespace DirectX;
using std::cout;

std::wstring GetExePath();

void UpdateWindowTitle(const std::wstring& appName);

// the return structure, with these values, you should be able to calculate the impulse
struct CollisionInfo
{
	bool isValid;                          // whether there is a collision point, true for yes
	DirectX::XMVECTOR collisionPointWorld; // the position of the collision point in world space
	DirectX::XMVECTOR normalWorld;         // the direction of the impulse to A, negative of the collision face of A
	//if the normalWorld == XMVectorZero(), no collision
	float colDist;
};

//Operator overloads
XMFLOAT3 operator+ (XMFLOAT3 left, XMFLOAT3 right);
XMFLOAT3 operator- (XMFLOAT3 left, XMFLOAT3 right);
XMFLOAT3 operator* (float scalar, XMFLOAT3 vector);
XMFLOAT3 operator* (XMFLOAT3 vector, float scalar);
XMFLOAT3 operator/ (XMFLOAT3 vector, float scalar);
std::ostream& operator<< (std::ostream& left, XMFLOAT3 right);
bool operator== (XMFLOAT3 left, XMFLOAT3 right);
bool operator!= (XMFLOAT3 left, XMFLOAT3 right);

//helper methods
float magnitude(XMFLOAT3 vector);
float dot(XMFLOAT3 a, XMFLOAT3 b);
XMFLOAT3 cross(XMFLOAT3 a, XMFLOAT3 b);
bool vectorsEqual(XMFLOAT3 a, XMFLOAT3 b);


CollisionInfo checkCollision(const XMMATRIX obj2World_A, const XMMATRIX obj2World_B);
CollisionInfo checkCollision(const XMMATRIX obj2World_A, const XMVECTOR worldPoint);
#endif
