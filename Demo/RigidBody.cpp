#include "RigidBody.h"

using namespace DirectX;


//Physical behaviour
void RigidBody::applyForceAtPoint(XMFLOAT3 force, XMFLOAT3 worldSpacePoint)
{
	//calculate point in local space (relative to center of mass)
	XMFLOAT3 relativePoint = worldSpacePoint - position;

	linearForce = linearForce + force;//accumulate linear force
	torque = torque + cross(relativePoint, force);
}

void RigidBody::applyForceAtPoint(XMFLOAT3 force, int index)
{
	if (index < massPoints.size() && index >= 0)
	{
		applyForceAtPoint(force, massPoints[index].position_world);
	}
}

void RigidBody::applyForceAtAllPoints(XMFLOAT3 force)
{
	for (int i = 0; i < massPoints.size(); i++)
	{
		applyForceAtPoint(force, massPoints[i].position_world);
	}
}

void RigidBody::applyLinearForce(XMFLOAT3 force)
{
	this->linearForce = this->linearForce + force;
}

void RigidBody::applyImpulseAtPoint(XMFLOAT3 collisionPointWorld, XMFLOAT3 collisionNormal, float impulse)
{
	//calculate collision point relative to rigidbodies center of mass
	XMFLOAT3 relativeCollisionPoint = collisionPointWorld - position;

	linearVelocity = linearVelocity + impulse * collisionNormal * massInverse;
	angularMomentum = angularMomentum + cross(relativeCollisionPoint, impulse * collisionNormal);
}

void RigidBody::applyImpulseAtPoint(int index, XMFLOAT3 collisionNormal, float impulse)
{
	if (index < massPoints.size() && index >= 0)
	{
		applyImpulseAtPoint(massPoints[index].position_world, collisionNormal, impulse);
	}
}

void RigidBody::applyImpulseAtAllPoints(XMFLOAT3 collisionNormal, float impulse)
{
	for (int i = 0; i < massPoints.size(); i++)
	{
		applyImpulseAtPoint(massPoints[i].position_world, collisionNormal, impulse);
	}
}

void RigidBody::immobilize()
{
	massInverse = 0;
	initial_inertiaTensorInverse = XMFLOAT3X3(0, 0, 0, 0, 0, 0, 0, 0, 0);
}

void RigidBody::update(float timeStep)
{
	//forces should already have been applied

	//Euler Step
	updatePosition(timeStep);
	updateLinearVelocity(timeStep);

	//Update Rotation (Quaternion)
	updateOrientation(timeStep);


	updateAngularMomentum(timeStep);// L

	updateInertiaTensorInverse();

	updateAngularVelocity();// w


	//update positions and velocities of points
	updateMassPoints();

	//everything updated --> clear linearForce and torque
	linearForce = XMFLOAT3(0, 0, 0);
	torque = XMFLOAT3(0, 0, 0);
}


//Constructors
RigidBody::RigidBody()
{
}

RigidBody::RigidBody(std::vector<Point> points, XMFLOAT3 scale, XMFLOAT3 initial_pos, XMFLOAT3 initial_rot_euler, XMFLOAT3 initial_linearVelocity, XMFLOAT3 initial_angularMomentum)//constructor
{
	float totalMass = 0.0f;

	//add up point masses
	for (int i = 0; i < points.size(); i++)
	{
		totalMass += points[i].mass;
	}

	this->massInverse = 1.0f / totalMass;


	//calculate center of mass
	XMFLOAT3 massCenter = XMFLOAT3(0, 0, 0);

	for (int i = 0; i < points.size(); i++)
	{
		massCenter = massCenter + points[i].position_world * points[i].mass;
	}

	massCenter = massCenter / totalMass;
	this->position = massCenter;


	massPoints = std::vector<Point>(points);//copy points vector to rigidbody


	//calculate local space position for all points
	for (int i = 0; i < massPoints.size(); i++)
	{
		massPoints[i].position_local = massPoints[i].position_world - position;
	}

	this->scale = scale;//init scale BEFORE inertiaTensorInverse

	initInertiaTensorInverse();


	//Initialization

	position = initial_pos;
	angularMomentum = initial_angularMomentum;
	linearVelocity = initial_linearVelocity;
	linearForce = XMFLOAT3(0, 0, 0);
	torque = XMFLOAT3(0, 0, 0);

	XMVECTOR initial_rot = XMQuaternionRotationRollPitchYaw(initial_rot_euler.x, initial_rot_euler.y, initial_rot_euler.z);
	XMStoreFloat4(&orientation, initial_rot);

	updateInertiaTensorInverse();
	updateAngularVelocity();

	updateMassPoints();
}


//Getters

int RigidBody::getMassPoint(XMFLOAT3 worldPosition)
{
	Point observedPoint;

	float searchSpaceRadius = 0.001f;

	//search point
	for (int i = 0; i < massPoints.size(); i++)
	{
		if ((massPoints[i].position_world.x - worldPosition.x) < searchSpaceRadius
			&& (massPoints[i].position_world.x - worldPosition.x) > -searchSpaceRadius
			&& (massPoints[i].position_world.y - worldPosition.y) < searchSpaceRadius
			&& (massPoints[i].position_world.y - worldPosition.y) > -searchSpaceRadius
			&& (massPoints[i].position_world.z - worldPosition.z) < searchSpaceRadius
			&& (massPoints[i].position_world.z - worldPosition.z) > -searchSpaceRadius)
		{
			return i;
		}
	}
	//if not found
	return -1;
}

Point RigidBody::getMassPoint(int index)
{
	return massPoints[index];
}


XMMATRIX RigidBody::getRotation()
{
	return XMMatrixRotationQuaternion(XMLoadFloat4(&orientation));
}

XMFLOAT3 RigidBody::getPosition()
{
	return position;
}

std::vector<XMFLOAT3> RigidBody::getPointPositions()
{
	std::vector<XMFLOAT3> result;

	for (int i = 0; i < massPoints.size(); i++)
	{
		result.push_back(massPoints[i].position_world);
	}

	return result;
}


XMFLOAT3X3 RigidBody::getInverseInertiaTensor()
{
	return inertiaTensorInverse;
}

float RigidBody::getInverseMass()
{
	return massInverse;
}

XMFLOAT3 RigidBody::getLinearVelocity()
{
	return linearVelocity;
}

XMFLOAT3 RigidBody::getAngularVelocity()
{
	return this->angularVelocity;
}


//static functions
float RigidBody::calculateImpulse(RigidBody a, RigidBody b, XMFLOAT3 collisionNormal, XMFLOAT3 collisionPointWorld, float c)//this function is ugly, don't look at it
{
	//called on collision of a point of b with object a

	//get required variables
	XMVECTOR colPoint_a = XMLoadFloat3(&(collisionPointWorld - a.getPosition()));
	XMVECTOR colPoint_b = XMLoadFloat3(&(collisionPointWorld - b.getPosition()));
	XMVECTOR n = XMLoadFloat3(&collisionNormal);
	XMMATRIX I_inv_a = XMLoadFloat3x3(&a.getInverseInertiaTensor());
	XMMATRIX I_inv_b = XMLoadFloat3x3(&b.getInverseInertiaTensor());


	//calculate relative velocity
	XMFLOAT3 contactVelocity_a = a.getLinearVelocity() + cross(collisionPointWorld - a.getPosition(), a.getAngularVelocity());
	XMFLOAT3 contactVelocity_b = b.getLinearVelocity() + cross(collisionPointWorld - b.getPosition(), b.getAngularVelocity());

	XMFLOAT3 relativeVelocity = (contactVelocity_b - contactVelocity_a);

	if (dot(relativeVelocity, collisionNormal) <= 0.0f)
	{
		//calculate "angular impulses"
		XMVECTOR angularImpulse_a = XMVector3TransformNormal(XMVector3Cross(colPoint_a, n), I_inv_a);
		XMVECTOR angularImpulse_b = XMVector3TransformNormal(XMVector3Cross(colPoint_b, n), I_inv_b);

		//calculate numerator
		float numerator = -(1.0f + c) * dot(relativeVelocity, collisionNormal);

		//calculate denominator
		XMFLOAT3 temp;
		XMStoreFloat3(&temp, XMVector3Cross(angularImpulse_a, colPoint_a) + XMVector3Cross(angularImpulse_b, colPoint_b));

		float denominator = a.getInverseMass() + b.getInverseMass() + dot(temp, collisionNormal);

		//calculate final impulse
		float J = numerator / denominator;

		return J;
	}
	else return 0.0f;
}


/*
void RigidBodySystem::HandleCollision(RigidBody* bodyA, RigidBody* bodyB, CollisionInfo collision)
{

	//Points of BodyB are in BodyA
	//Calculate Impulse:
	Vector3 contactPointA = collision.collisionPointWorld - bodyA->Position;
	Vector3 contactPointB = collision.collisionPointWorld - bodyB->Position;

	Vector3 contactVelocityA = bodyA->Velocity + contactPointA.Cross(bodyA->AngularVelocity);
	Vector3 contactVelocityB = bodyB->Velocity + contactPointB.Cross(bodyB->AngularVelocity);

	Vector3 relativeVelocity = contactVelocityB - contactVelocityA;

	if (relativeVelocity.Dot(collision.normalWorld) > 0)
	{
		float bounciness = (bodyA->Bounciness + bodyB->Bounciness) / 2.f;
		float top = ((1 + bounciness) * relativeVelocity).Dot(collision.normalWorld);
		float inverseMass = 1 / bodyA->TotalMass + 1 / bodyB->TotalMass;
		Vector3 inertiaTensoredNormalA = Vector3::Transform(contactPointA.Cross(collision.normalWorld), bodyA->InversedInertiaTensor).Cross(contactPointA);
		Vector3 inertiaTensoredNormalB = Vector3::Transform(contactPointB.Cross(collision.normalWorld), bodyB->InversedInertiaTensor).Cross(contactPointB);
		float impulse = top / (inverseMass + (inertiaTensoredNormalA + inertiaTensoredNormalB).Dot(collision.normalWorld));

		Vector3 knockback = (impulse * collision.normalWorld) / (float)collision.numberOfTotalCollisions;

		bodyA->Velocity += Vector3(knockback / bodyA->TotalMass);
		bodyB->Velocity -= Vector3(knockback / bodyB->TotalMass);

		//std::cout << bodyA->Velocity.x << std::endl;

		bodyA->Momentum += contactPointA.Cross(knockback);
		bodyB->Momentum -= contactPointB.Cross(knockback);

		//std::cout << "contactPointA.Cross(knockback) " << contactPointA.Cross(knockback).x << " " << contactPointA.Cross(knockback).y << " " << contactPointA.Cross(knockback).z << std::endl;
		//std::cout << "contactPointB.Cross(knockback) " << contactPointB.Cross(knockback).x << " " << contactPointB.Cross(knockback).y << " " << contactPointB.Cross(knockback).z << std::endl;


	}
}
*/



//Internal physics updates
void RigidBody::updateOrientation(float timeStep)
{
	//Convert into XMVECTOR
	XMVECTOR currentOrientation = XMLoadFloat4(&orientation);

	XMFLOAT4 angVelocity = XMFLOAT4(angularVelocity.x, angularVelocity.y, angularVelocity.z, 0);
	XMVECTOR angVelQuaternion = XMLoadFloat4(&angVelocity);// --> (0, w)

	//Integrate Orientation
	currentOrientation += (timeStep/2.0f) * XMQuaternionMultiply(angVelQuaternion, currentOrientation);// r = r + h/2 * (0, w) * r

	//re-normalize
	currentOrientation = XMQuaternionNormalize(currentOrientation);

	XMStoreFloat4(&orientation, currentOrientation);//store in RigidBody variable
}

void RigidBody::updateAngularVelocity()
{
	XMVECTOR result;

	//Convert to XMMATRIX/XMVECTOR
	XMMATRIX inertiaTensorInv = XMLoadFloat3x3(&inertiaTensorInverse);
	XMVECTOR angMomentum = XMLoadFloat3(&angularMomentum);

	result = XMVector3Transform(angMomentum, inertiaTensorInv);// w = I^-1 * L

	XMStoreFloat3(&angularVelocity, result);
}

void RigidBody::updateInertiaTensorInverse()
{
	//Get Rotation Matrix from orientation Quaternion
	XMMATRIX rot = XMMatrixRotationQuaternion(XMLoadFloat4(&orientation));
	XMMATRIX rot_Inv = XMMatrixInverse(nullptr, rot);

	//get initial inverse inertia tensor as XMMATRIX
	XMMATRIX inertiaTensorInverse_0 = XMLoadFloat3x3(&initial_inertiaTensorInverse);

	XMMATRIX result = rot_Inv * inertiaTensorInverse_0 * rot;// I^-1 = Rot(r) * I_0 ^-1 * Rot(r)^-1

	DirectX::XMStoreFloat3x3(&inertiaTensorInverse, result);
}

void RigidBody::updateAngularMomentum(float timeStep)
{
	angularMomentum = angularMomentum + timeStep * torque;
}

void RigidBody::updatePosition(float timeStep)
{
	position = position + timeStep * linearVelocity;
}

void RigidBody::updateLinearVelocity(float timeStep)
{
	linearVelocity = linearVelocity + timeStep * linearForce * massInverse;
}

void RigidBody::updateMassPoints()
{
	//Get Rotation Matrix
	XMMATRIX rot = XMMatrixRotationQuaternion(XMLoadFloat4(&orientation));

	//for every mass point, update world space position and velocity
	for (int i = 0; i < massPoints.size(); i++)
	{
		XMVECTOR worldspace_pos = XMLoadFloat3(&position) + XMVector3Transform(XMLoadFloat3(&massPoints[i].position_local), rot);
		XMStoreFloat3(&massPoints[i].position_world, worldspace_pos);

		massPoints[i].velocity_world = linearVelocity + cross(angularVelocity, massPoints[i].position_local);

	}
}

void RigidBody::initInertiaTensorInverse()
{
	/*
	// assumption: homogenous cuboid
	const float x = scale.x;
	const float y = scale.y;
	const float z = scale.z;
	const float xSquared = x * x;
	const float ySquared = y * y;
	const float zSquared = z * z;

	XMMATRIX I_inverted(
		12.0f * massInverse / (ySquared + zSquared), 0.0f, 0.0f, 0.0f,
		0.0f, 12.0f * massInverse / (xSquared + zSquared), 0.0f, 0.0f,
		0.0f, 0.0f, 12.0f * massInverse / (xSquared + ySquared), 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
		);
	
	DirectX::XMStoreFloat3x3(&initial_inertiaTensorInverse, I_inverted);*/

	/*
	XMFLOAT3X3 testTensor = XMFLOAT3X3(0, 0, 0, 0, 0, 0, 0, 0, 0);
	testTensor._11 = 0.02083f; testTensor._22 = 0.052083f; testTensor._33 = 0.052083f;

	XMMATRIX inverted = XMMatrixInverse(nullptr, XMLoadFloat3x3(&testTensor));
	DirectX::XMStoreFloat3x3(&initial_inertiaTensorInverse, inverted);*/

	
	XMMATRIX I = XMLoadFloat3x3(&XMFLOAT3X3(0, 0, 0, 0, 0, 0, 0, 0, 0));

	for (int n = 0; n < massPoints.size(); n++)
	{
		//get values of point n
		XMFLOAT3 pos = massPoints[n].position_local;
		float mass = massPoints[n].mass;

		//compute (x_n * x_n^T)
		XMMATRIX C = XMLoadFloat3x3(&XMFLOAT3X3(pos.x * pos.x, pos.x * pos.y, pos.x * pos.z,
												pos.y * pos.x, pos.y * pos.y, pos.y * pos.z,
												pos.z * pos.x, pos.z * pos.y, pos.z * pos.z));

		//compute trace
		XMMATRIX trace = XMMatrixIdentity() * (pos.x * pos.x + pos.y * pos.y + pos.z * pos.z);

		XMMATRIX I_n = mass * (trace - C);

		I = I + I_n;//add up matrices for all points
	}

	XMMATRIX I_inverted = XMMatrixInverse(nullptr, I);//invert I
	DirectX::XMStoreFloat3x3(&initial_inertiaTensorInverse, I_inverted);
	

	/*
	XMFLOAT3X3 C;

	for (int j = 0; j < 3; j++)
	{
		for (int k = 0; k < 3; k++)
		{
			float sum = 0;

			//sum for all mass points
			for (int n = 0; n < massPoints.size(); n++)
			{
				float x1, x2;

				//calculate x1
				switch (j)
				{
					case 0:
						x1 = massPoints[n].position_local.x;
						break;
					case 1:
						x1 = massPoints[n].position_local.y;
						break;
					case 2:
						x1 = massPoints[n].position_local.z;
						break;
				}

				//calculate x2
				switch (k)
				{
					case 0:
						x2 = massPoints[n].position_local.x;
						break;
					case 1:
						x2 = massPoints[n].position_local.y;
						break;
					case 2:
						x2 = massPoints[n].position_local.z;
						break;
				}

				sum += massPoints[n].mass * x1 * x2;
			}

			C.m[j][k] = sum;// save in matrix
		}

	}

	//trace(C)
	float trace = C._11 + C._22 + C._33;

	//Id trace
	XMFLOAT3X3 Id_trace = XMFLOAT3X3(0, 0, 0, 0, 0, 0, 0, 0, 0);
	Id_trace._11 = trace; Id_trace._22 = trace; Id_trace._33 = trace;

	XMMATRIX I = XMLoadFloat3x3(&Id_trace) - XMLoadFloat3x3(&C);//calculate inertia tensor

	XMMATRIX I_inverted = XMMatrixInverse(nullptr, I);//invert I
	DirectX::XMStoreFloat3x3(&initial_inertiaTensorInverse, I_inverted);

	*/
	
}
