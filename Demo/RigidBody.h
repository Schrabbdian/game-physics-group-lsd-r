#pragma once
#include <DirectXMath.h>
#include "util\util.h"
#include <vector>

using namespace DirectX;

struct Point
{
	XMFLOAT3 position_world;
	XMFLOAT3 position_local;

	XMFLOAT3 velocity_world;
	float mass;

	Point()
	{
	}

	Point(XMFLOAT3 pos_world, float m)
	{
		position_world = pos_world;
		mass = m;
	}
};

class RigidBody
{
	//Attributes
private:
	std::vector<Point> massPoints;
	float massInverse;// 1/m, where m = sum of all point masses
	XMFLOAT3 scale;

	//positional
	XMFLOAT3 position;//of the center of mass
	XMFLOAT3 linearVelocity;// - " -

	//rotational
	XMFLOAT4 orientation;// <-- a quaternion
	XMFLOAT3 angularVelocity;
	XMFLOAT3 angularMomentum;
	XMFLOAT3X3 inertiaTensorInverse;// "angular mass: how easy is it to rotate around a certain axis"

	//forces
	XMFLOAT3 linearForce;
	XMFLOAT3 torque;//"angular force"

	XMFLOAT3X3 initial_inertiaTensorInverse;

	//Functions
public:

	//physical behaviour
	void applyForceAtPoint(XMFLOAT3 force, XMFLOAT3 worldSpacePoint);
	void RigidBody::applyForceAtPoint(XMFLOAT3 force, int index);
	void RigidBody::applyForceAtAllPoints(XMFLOAT3 force);
	void RigidBody::applyLinearForce(XMFLOAT3 force);

	void applyImpulseAtPoint(XMFLOAT3 collisionPointWorld, XMFLOAT3 collisionNormal, float impulse);
	void applyImpulseAtPoint(int index, XMFLOAT3 collisionNormal, float impulse);
	void applyImpulseAtAllPoints(XMFLOAT3 collisionNormal, float impulse);
	void immobilize();

	void update(float timeStep);

	//constructors
	RigidBody();
	RigidBody(std::vector<Point> points, XMFLOAT3 scale, XMFLOAT3 initial_pos, XMFLOAT3 initial_rot_euler, XMFLOAT3 initial_linearVelocity, XMFLOAT3 initial_angularMomentum);//constructor

	//getters
	int getMassPoint(XMFLOAT3 worldPosition);
	Point getMassPoint(int index);
	XMMATRIX getRotation();
	XMFLOAT3 getPosition();
	std::vector<XMFLOAT3> getPointPositions();
	XMFLOAT3X3 getInverseInertiaTensor();
	float getInverseMass();
	XMFLOAT3 getLinearVelocity();
	XMFLOAT3 getAngularVelocity();


	//static functions
	static float calculateImpulse(RigidBody a, RigidBody b, XMFLOAT3 collisionNormal, XMFLOAT3 collisionPointWorld, float c);

private:
	void updateOrientation(float timeStep);

	void updateAngularVelocity();

	void updateInertiaTensorInverse();

	void updateAngularMomentum(float timeStep);

	void updatePosition(float timeStep);

	void updateLinearVelocity(float timeStep);

	void updateMassPoints();

	void initInertiaTensorInverse();
};