#pragma once
#include <DirectXMath.h>
#include "util\util.h"
#include <vector>
#include "RigidBody.h"
#include <GeometricPrimitive.h>
#include "Effects.h"

using namespace DirectX;

class RigidBox
{
	//Attributes
private:
	XMFLOAT3 position;
	XMFLOAT3 scale;
	float mass;

public:
	RigidBody rigidBody;

public:
	XMFLOAT3 color = XMFLOAT3(1.0f, 1.0f, 1.0f);


	//Functions
public:
	void configureDraw(DirectX::BasicEffect* effect);
	void update(float timeStep);
	RigidBox();
	RigidBox(XMFLOAT3 position, XMFLOAT3 scale, float mass, XMFLOAT3 rotation, XMFLOAT3 linearVelocity, XMFLOAT3 angularMomentum);

	XMMATRIX getWorldMatrix();
	
};