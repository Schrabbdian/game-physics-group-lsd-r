#include "Blob.h"

using namespace DirectX::SimpleMath;

Blob::Blob(Vector3 pos)
{
	position = pos;
	volume = (4.0f / 3.0f) * M_PI * pow(radius, 3);
	//restDensity = mass / volume; //TODO: Just for testing
}

//just for collision grid
Blob::Blob()
{
	volume = (4.0f / 3.0f) * M_PI * pow(radius, 3);
}

Blob::~Blob()
{
}

float Blob::calculateDensity(std::vector<Blob*> blobsToInterpolate, float kernelSize)
{
	//interpolate density
	float interpolatedDensity = 0;
	for (int i = 0; i < blobsToInterpolate.size(); i++) //folie 24
	{
		//smoothing kernel, folie 25
		float q = (position - blobsToInterpolate[i]->position).Length() / kernelSize;
		/*
		float kernel = 3.0f / (2.0f * pow(kernelSize, 3) * M_PI);
		if (q >= 0 && q < 1)
		{
			kernel *= (2.0f / 3.0f) - pow(q, 2) + 0.5f * pow(q, 3);
		}
		else if (q >= 1 && q < 2)
		{
			kernel *= (1.0f / 6.0f) * pow((2.0f - q), 3);
		}
		else if (q >= 2)
		{
			kernel = 0;
		}

		interpolatedDensity += blobsToInterpolate[i].mass * kernel;
		*/
		/*
		//test with range [0,1]
		float kernelMax = (2.0f / 3.0f) * (3.0f / (2.0f * pow(kernelSize, 3) * M_PI));
		kernel = kernel / kernelMax;
		*/
		
		float kernel = 1.0f / (pow(kernelSize, 3) * M_PI);

		if (q >= 0 && q < 1)
		{
			kernel *= (1.0f / 4.0f) * pow((2.0f - q), 3) - pow((1.0f - q), 3);
		}
		else if (q >= 1 && q < 2)
		{
			kernel *= (1.0f / 4.0f) * pow((2.0f - q), 3);
		}
		else if (q >= 2)
		{
			kernel = 0;
		}
		
		interpolatedDensity += blobsToInterpolate[i]->mass * kernel;
	}

	this->density = interpolatedDensity;
	return interpolatedDensity;
}

float Blob::calculatePressure(float stiffness)
{
	float pressure = stiffness * (pow((density / restDensity), 7) - 1); //folie 29
	this->pressure = pressure;
	return pressure;
}

Vector3 Blob::calculatePressureForce(std::vector<Blob*> blobsToInterpolate, float kernelSize, int thisBlobIndex)
{
	Vector3 pressureForce = Vector3(0, 0, 0);

	for (int i = 0; i < blobsToInterpolate.size(); i++) //folie 29
	{
		if (blobsToInterpolate[i]->id != this->id)
		{
			//float a_i = blobsToInterpolate[i].mass;
			//float density_i = blobsToInterpolate[i].restDensity;
			//float density_i = blobsToInterpolate[i].density;

			//smoothing kernel, folie 25
			float q = (position - blobsToInterpolate[i]->position).Length() / kernelSize;
			/*
			float kernel = 9.0f / (4.0f * pow(kernelSize, 5) * M_PI);
			if (q >= 0 && q < 1)
			{
				kernel *= (q - (4.0f / 3.0f)) * q * kernelSize;
			}
			else if (q >= 1 && q < 2)
			{
				kernel *= -pow((2.0f - q), 2) * (kernelSize / 3);
			}
			else if (q >= 2)
			{
				kernel = 0;
			}
			*/

			/*
			float kernelMax = (9.0f / (4.0f * pow(kernelSize, 5) * M_PI)) * (0.5f - (4.0f / 3.0f)) * 0.5f * kernelSize;
			kernel = kernel / kernelMax;
			*/
			
			float kernel = 1.0f / (pow(kernelSize, 3) * M_PI);

			if (q >= 0 && q < 1)
			{
				kernel *= (-3.0f / 4.0f) * pow((2.0f - q), 2) + 3.0f * pow((1.0f - q), 2);
			}
			else if (q >= 1 && q < 2)
			{
				kernel *= (-3.0f / 4.0f) * pow((2.0f - q), 2);
			}
			else if (q >= 2)
			{
				kernel = 0;
			}
			


			Vector3 normalizedConnectionVector;
			(position - blobsToInterpolate[i]->position).Normalize(normalizedConnectionVector);

			Vector3 triangle_w = normalizedConnectionVector * kernel;  //this isn't even my final name

			//pressureForce += ((blobsToInterpolate[i].pressure + this->pressure) / 2.0f) * (this->mass / blobsToInterpolate[i].restDensity) * triangle_w;
			//pressureForce += ((blobsToInterpolate[i].pressure + this->pressure) / 2.0f) * (this->mass / blobsToInterpolate[i].density) * triangle_w;
			pressureForce += ((blobsToInterpolate[i]->pressure + this->pressure) / 2.0f) * (blobsToInterpolate[i]->mass / blobsToInterpolate[i]->density) * triangle_w;
		}
	}

	force += pressureForce;
	return force;
}


